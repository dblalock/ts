#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

from ..utils import arrays as ar
from viz_utils import plotRect, removeEveryOtherYTick

def plotSeqAndFeatures(seq, X, model, createModelAx=False, padBothSides=False,
	capYLim=1000, compressVertically=False):
	"""plots the time series above the associated feature matrix"""

	# mpl.rcParams['font.size'] = 30 # tick label size

	fsize = (10, 6) if compressVertically else (10, 8)
	plt.figure(figsize=fsize)
	# plt.figure(figsize=(8, 10))
	if createModelAx:
		nRows = 3 if compressVertically else 4
		nCols = 7
		axSeq = plt.subplot2grid((nRows,nCols), (0,0), colspan=(nCols-1))
		axSim = plt.subplot2grid((nRows,nCols), (1,0), colspan=(nCols-1), rowspan=(nRows-1))
		axPattern = plt.subplot2grid((nRows,nCols), (1,nCols-1), rowspan=(nRows-1))
		axes = (axSeq, axSim, axPattern)
	else:
		nRows = 4
		nCols = 1
		axSeq = plt.subplot2grid((nRows,nCols), (0,0))
		axSim = plt.subplot2grid((nRows,nCols), (1,0), rowspan=(nRows-1))
		axes = (axSeq, axSim)

	for ax in axes:
		ax.autoscale(tight=True)

	axSeq.plot(seq)
	yMax = seq.max() * 1.1 if compressVertically else seq.max()
	axSeq.set_ylim([seq.min(), min(capYLim, yMax)])

	if padBothSides:
		padLen = (len(seq) - X.shape[1]) // 2
		Xpad = ar.addZeroCols(X, padLen, prepend=True)
		Xpad = ar.addZeroCols(Xpad, padLen, prepend=False)
	else:
		padLen = len(seq) - X.shape[1]
		Xpad = ar.addZeroCols(Xpad, padLen, prepend=False)
	axSim.imshow(Xpad, interpolation='nearest', aspect='auto')

	titleFontSize = 22 if compressVertically else 28
	ySim = 1.03 if compressVertically else 1.01
	axSeq.set_title("Time Series", fontsize=titleFontSize, y=1.04)
	axSim.set_title("Feature Matrix", fontsize=titleFontSize, y=ySim)

	# plot the learned pattern model
	if createModelAx:
		modelFontSize = 20 if compressVertically else 24
		axPattern.set_title("Learned\nPattern", fontsize=modelFontSize, y=1.02)

		# if compressVertically:
		# 	# XXX the x3 is based on how much viz.figs.makeOutputFig
		# 	# is downsampling atm
		# 	tickLocations = np.linspace(0, Xpad.shape[0] * 3, 16, dtype=np.int)
		# 	tickLocations = plt.FixedLocator(tickLocations)
		# 	axSim.yaxis.set_major_locator(tickLocations)

		if model is not None:
			axPattern.imshow(model, interpolation='nearest', aspect='auto')
			tickLocations = plt.FixedLocator([0, model.shape[1]])
			axPattern.xaxis.set_major_locator(tickLocations)
			axPattern.yaxis.tick_right() # y ticks on right side
		else:
			print("WARNING: attempted to plot None as feature weights!")

	for ax in axes:
		for tick in ax.get_xticklabels() + ax.get_yticklabels():
			tick.set_fontsize(20)
	removeEveryOtherYTick(axSeq)

	# if compressVertically:
	# 	plt.subplots_adjust(hspace=0)

	return axes


def plotFFOutput(ts, startIdxs, endIdxs, featureMat, model,
	cleanOverlap=False, highlightInFeatureMat=False,
	showGroundTruth=False, **kwargs):

	axSeq, axSim, axPattern = plotSeqAndFeatures(ts.data, featureMat, model,
		createModelAx=True, padBothSides=True, **kwargs)

	# our algorithm is allowed to return overlapping regions, but
	# they look visually cluttered, so crudely un-overlap them
	if cleanOverlap:
		for i in range(1, len(startIdxs)):
			start, end = startIdxs[i], endIdxs[i-1]
			if start < end:
				middle = int((start + end) // 2)
				startIdxs[i] = middle + 1
				endIdxs[i-1] = middle

	# plot estimated regions
	for startIdx, endIdx in zip(startIdxs, endIdxs):
		plotRect(axSeq, startIdx, endIdx)

		if highlightInFeatureMat:
			# this is a hack to plot where instances are in the feature
			# matrix; ideally, this info should come directly from
			# the learning function
			matOffset = 0 # 0, not below line, since we told it to pad both sides
			matStartIdx = startIdx - matOffset
			matEndIdx = endIdx - matOffset
			plotRect(axSim, matStartIdx, matEndIdx, alpha=.2)

	# plot ground truth regions
	if showGroundTruth:
		for startIdx, endIdx in zip(ts.startIdxs, ts.endIdxs):
			plotRect(axSeq, startIdx, endIdx, color='none', hatch='---', alpha=.3)

	axSim.set_xlabel("Time")
	axSim.set_ylabel("Feature")
	plt.tight_layout()
