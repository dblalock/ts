% To formalize our task, we introduce the following definitions.
% \vspace{-1mm}
\begin{Definition}{
\textbf{Time Series}. A $D$-variable time series $T$ of length $N$ is a sequence of real-valued vectors $t_1,\ldots,t_N, t_i \in \mathbb{R}^D$. If $D = 1$, we call $T$ ``univariate'', and if $D > 1$, we call $T$ ``multivariate.''
}
\vspace{-2mm}
\end{Definition}
\begin{Definition}{\textbf{Region}. A region $R$ is a pair of indices $(a, b), a \le b$. The value $b - a + 1$ is termed the \textbf{length} of the region, and the time series $t_a,\ldots,t_b$ is the \textbf{subsequence} for that region. If a region reflects an occurrence of the event, we term the region an event \textbf{instance}.
}
\end{Definition}

% ------------------------------------------------
\vspace{-2.5mm}
\subsection{Problem Statement}
\vspace{-.5mm}
% ------------------------------------------------
We seek the set of regions that are most likely to have come from a shared ``event'' distribution rather than a ``noise'' distribution. This likelihood is assessed based on the subset of features maximizing how distinct these distributions are (using some fixed feature representation).

Formally, let $x_1,\ldots,x_K$ be binary feature representations of all $K$ possible regions in a given time series and $x_i^j$ denote feature $j$ in the region $i$. We seek the optimal set of regions $\mathcal{R}^{\ast}$, defined as:
\begin{align} \label{eq:concreteObjective}
	\mathcal{R}^{\ast}= \argmax_{\mathcal{R}} \max_{\mathcal{F}} p(\mathcal{R}) \sum_{j \in \mathcal{F}} c_j (log(\theta_{1j}) - log(\theta_{0j}))
\end{align}
where $\theta_{0j}$ and $\theta_{1j}$ are the empirical probabilities for each feature $j$ in the whole time series and the regions $\mathcal{R}$ respectively, and $c_j$ is the count of feature $j$, ${\sum_{i \in \mathcal{R}} x_i^j}$. $\mathcal{F}$ is the set of features that best separate the event. The prior $p(\mathcal{R})$ is 0 if regions overlap too heavily or violate certain length bounds (see below) and is otherwise uniform.

Equation~\ref{eq:concreteObjective} says that we would like to find regions $i$ and features $j$ such that $x_i^j$ happens both many times (so that $c_j$ is large) and much more often than would occur by chance (so that $log(\theta_{1j}) - log(\theta_{0j})$ is large). In other words, the best features $\mathcal{F}$ are the largest set that consistently occurs across the most regions, and $\mathcal{R}^{\ast}$ is these regions.

Given certain independencies, this objective is a MAP estimate of the regions and features. Because of space constraints, we defer the details to \cite{extractWebsite}.

% ------------------------------------------------
\vspace{-1mm}
\subsection{Assumptions}
% ------------------------------------------------
\hspace{-10pt}We do \textit{not} make any of the following common assumptions: % no indent
% \vspace{-5mm}
\begin{itemize}
% \itemsep.2mm  % was this in submitted version
\itemsep-.1mm
% \item A known number of instances. %\cite{neverEnding}
% \item A known or constant length for instances.
% \item A known or regular spacing between instances.
\item A known or constant length for instances, a known or regular spacing between instances, or a known number of instances.
\item A known set of characteristics shared by instances. In particular, we do not assume that all instances have the same mean and variance, so we cannot bypass normalization when making similarity comparisons.
\item That there is only one variable, or that all variables are affected by the event.
\item Anything about variables not affected by the event.
\end{itemize}

\hspace{-10pt}So that the problem is well-defined, we \textit{do} assume that:
% Since one of our contributions is relaxing many assumptions made by existing work, we enumerate them in some detail.
\vspace{-.5mm}
\begin{itemize}
% \itemsep.2mm  % was this in submitted version
\itemsep-.1mm
\item The time series contains instances of only one class of event. It may contain other transient phenomena, but we take our weak label to mean (only) that the primary structure in the time series comes from the events of the labeled class and that there are no other repeating events.
\item There are at least two instances of the event, and each instance produces some characteristic but unknown pattern in the data.
\item There exist bounds $M_{min}$ and $M_{max}$, $M_{min} > M_{max}/2$ on the lengths of instances. These bounds disambiguate the case where pairs of adjacent instances could be viewed as single instances of a longer event. Similarly, no two instances overlap by more than ${M_{min}-1}$ time steps.
\end{itemize}
\vspace{-1mm}
We also do not consider datasets in which instances are rare \cite{rareMotif}---all time series used in our experiments have instances that collectively comprise $\sim$10\% of the data or more (though this exact number is not significant).

% ------------------------------------------------
\vspace{-1mm}
\subsection{Why the Task is Difficult}
% ------------------------------------------------

The lack of assumptions means that the number of possible sets of regions and relevant variables is intractably large. Suppose that we have a $D$-variable time series $T$ of length $N$ and $M_{min} \le M \le M_{max}$. There are up to $O(N/M)$ instances, which can collectively start at (at most) $\binom{N}{N/M}$ positions. Further, each can be of $O(M)$ different lengths. Finally, the event may affect any of $2^D - 1$ possible subsets of variables. Altogether, this means that there are roughly $O(N^{N/M} \cdot M^{N/M} \cdot 2^D)$ combinations of regions and variables.

Moreover, while there may be heuristics or engineered features that could allow isolation of any particular event in any particular domain, we seek to develop a general-purpose tool that requires no coding or tuning by humans. We therefore do not use such event-specific knowledge. This generality is both a convenience for human practitioners and a necessity for real-world deployment of a system that learns new events at runtime. % Our intended use case is to extract examples of new events and have the system recognize these events in the future. % Consequently, we cannot rely on prior knowledge about the data or events.
Lastly, because our aim is to extract examples for future use, we seek to locate full events, not merely the pieces that are easiest to find.
