
The rise of wearable technology and connected devices has made available a vast amount of sensor data, and with it the promise of improvements in everything from human health \cite{bsnSubdim} to user interfaces \cite{minnenAffixHMMs} to agriculture \cite{keoghInsect}. Unfortunately, the raw sequences of numbers comprising this data are often insufficient to offer value. For example, a smart watch user is not interested in their arm's acceleration signal, but rather in having their gestures or actions recognized.

Spotting such high-level events using low-level signals is challenging. Given enough labeled examples of the events taking place, one could, in principle, train a classifer for this purpose.
Unfortunately, obtaining labeled examples is an arduous task \cite{nuactiv, dataDicts, ushapelets, fastUshapelets}. While data such as images and text can be culled at scale from the internet, most time series data cannot. Furthermore, the uninterpretability of raw sequences of numbers often makes time series difficult or impossible for humans to annotate \cite{nuactiv}. % As \cite{nuactiv} describes, ``labeled samples are often time consuming and expensive to get, as they require huge effort from human annotators or experts.''

It is often possible, however, to obtain \textit{approximate} labels for particular stretches of time. The widely-used human action dataset of \cite{msrc12}, for example, consists of streams of data in which a subject is known to have performed a particular action roughly a certain number of times, but the exact starts and ends of each action instance are unknown. Furthermore, the recordings include spans of time that do not correspond to any action instance. Similarly, the authors of the Gun-Point dataset obtained recordings \textit{containing} different gestures, but had to expend considerable effort extracting each instance \cite{ushapelets}. This issue of knowing that there are examples \textit{within} a time series but not knowing \textit{where} in the data they begin and end is common \cite{msrc12, ushapelets, dataDicts, minnenSubdim, nuactiv}. % Indeed, it is the default situation when recording examples of temporal phenomena, unless one can precisely annotate their beginnings and ends as they happen. % TODO cite cmuMocap

To leverage these \textit{weak labels}, we developed an algorithm, EXTRACT, that efficiently isolates examples of an event given only a time series known to contain several occurrences of it. A simple illustration of the problem we consider is given in Figure~\ref{fig:fig1}. The various lines depict the (normalized) current, voltage, and other power measures of a home dishwasher. Shown are three instances of the dishwasher running, with idleness in between. With no prior knowledge or domain-specific tuning, our algorithm correctly determines not only what this repeating event looks like, but also where it begins and ends.
\vspace{-.5mm}
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/fig1}
	\vspace*{-4.5mm}
	\caption{\textit{a}) True instances of the dishwasher running (shaded). \textit{b}) Even when told the length and number of event instances, the recent algorithm of \cite{plmd} returns intervals with only the beginning of the event. \textit{c}) Our algorithm returns accurate intervals with no such prior knowledge.}
	\label{fig:fig1}
\end{center}
\end{figure}

\vspace{-1mm}
This is a challenging task, since the variables affected by the event, as well as the number, lengths, and positions of event instances, are all unknowns. Further, it is not even clear what objective should be maximized to find an event. For example, finding the nearest subsequences using the Euclidean distance yields the incorrect event boundaries returned by \cite{plmd} (Fig~\ref{fig:fig1}b).

To overcome these barriers, our technique leverages three observations:
\vspace{-.25mm}
\begin{enumerate}
\itemsep0em
\item Each subsequence of a time series can be seen as having representative features; for example, it may resemble different shapelets \cite{shapelets} or have a particular level of variance.
\item A repeating event will cause a disproportionate number of these features to occur together where the event happens. In Figure~\ref{fig:fig1}, for example, these features are a characteristic arrangement of spikes in the values of certain variables.
\item If we can identify these features, we can locate each instance of the event with high probability. This holds even in the presence of irrelevant variables (which merely fail to contribute useful features) and unknown instance lengths (which can be inferred based on the interval over which the features occur together).
\end{enumerate}

\hspace{-10pt}Our contributions consist of:
\vspace{-.25mm}
\begin{itemize}
\itemsep-.1mm
\item A formulation of semi-supervised event discovery in time series under assumptions consistent with real data. In particular, we allow multivariate time series, events that affect only subsets of variables, and instances of varying lengths.
\item An $O(N\log(N))$ algorithm to discover event instances under this formulation. It requires less than 300 lines of code and is fast enough to run on batches of data in real time. It is also considerably faster, and often much more accurate, than similar existing algorithms \cite{plmd, moen}. For example, we recognize instances of the above dishwasher pattern with an F1 score of over $90$\%, while the best-performing comparison \cite{plmd} achieves under $30$\%.
\item Open source code and labeled time series that can be used to reproduce and extend our work. In particular, we believe that our annotation of the full dishwasher dataset \cite{ampds} makes this the longest available sensor-generated time series with ground truth event start and end times.
\end{itemize}

% ``labeled samples are often time consuming and expensive to get, as they require huge effort from human annotators or experts.''

