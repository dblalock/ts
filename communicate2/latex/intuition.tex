
In this section we offer a high-level overview of our technique and the intuition behind it, deferring details to Section~\ref{sec:method}.

The basic steps are given in Algorithm~\ref{algo:extract}. In step 1, we create a representation of the time series that is invariant to instance length and enables rapid pruning of irrelevant dimensions. In step 2, we find sets of regions that may contain event instances. In step 3, we refine these sets to estimate the optimal instances $\mathcal{R}^\ast$.

Since the main challenge overcome by this technique is the lack of information regarding instance lengths, instance start positions, and relevant dimensions, we elaborate upon these steps by describing how they allow us to deal with each of these unknowns. We begin with a simplified approach and build to a sketch of the full algorithm. In particular, we begin by assuming that time series are one-dimensional, instances are nearly identical, and all features are useful.

\begin{algorithm}[h] % empty algo so that algo numbers stay correct
\caption{EXTRACT}
\label{algo:extract}
\end{algorithm}
\vspace{-3mm}
\begin{mdframed}
\begin{outline}[enumerate]
\1 \hspace{-1.5mm} \textbf{Transform the time series $T$ into a feature matrix $\Phi$}
\vspace{-.5mm}
	\2 Sample subsequences from the time series to use as shape features
	\2 Transform $T$ into $\Phi$ by encoding the presence of these shapes across time
	\2 Blur $\Phi$ to achieve length and time warping invariance

\1 \textbf{Using $\Phi$, generate sets of ``candidate'' windows that may contain event instances}
\vspace{-.5mm}
	\2 Find ``seed'' windows that are unlikely to have arisen by chance
	\2 Find ``candidate'' windows that resemble each seed
	\2 Rank candidates based on similarity to their seeds

\1 \textbf{Infer the true instances within these candidates}
\vspace{-.5mm}
	\2 Greedily construct subsets of candidate windows based on ranks
	\2 Score these subsets and select the best one
	\2 Infer exact instance boundaries within the selected windows
\end{outline}
\end{mdframed}

% ------------------------------------------------
\vspace{-2mm}
\subsection{Unknown Instance Lengths}
Like most existing work, we find event instances by searching over shorter regions of data within the overall time series (Fig \ref{fig:lengthsProblem}a). Since we do not know how long the instances are, this seemingly requires exhaustively searching regions of many lengths \cite{moen, plmd, ratanaFindAllCrap}, so that the instances are sure to be included in the search space.

However, there is an alternative. Our approach is to search over all regions of a \textit{single} length $M_{max}$ (the maximum possible instance length) and then refine these approximate regions. For the moment, we defer details of the refinement process. We refer to this set of regions searched as \textit{windows}, since they correspond to all positions of a sliding window over the time series.
% Our insight is that the regions returned by a search need not \textit{be} instances, but merely \textit{contain} instances. Concretely, our approach is to search regions of a \textit{single} length (slightly longer than the instances) and then infer the instance locations by finding commonalities in the returned set of regions.

This single-length approach presents a challenge: since windows longer than the instances will contain extraneous data, only parts of these windows will appear similar. As an example, consider Figure~\ref{fig:lengthsProblem}. Although the two windows shown contain identical sine waves, the noise around them causes the windows to appear different, as measured by Euclidean distance (the standard measure in most motif discovery work) (Fig~\ref{fig:lengthsProblem}b). Worse, because the data must be mean-normalized for this comparison to be meaningful \cite{mk}, it is not even clear what portions of the regions are similar or different---because the noise has altered the mean, even the would-be identical portions are offset from one another.
\vspace{-1mm}
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/lengthsProblem}
	\caption{a) A time series containing two event instances. b) Including extra data yields large distances (grey lines) between the windows $i$ and $j$ around the event instances. c) Comparing based on subsequences within these windows allows close matches between the pieces of the sine waves.}
	\label{fig:lengthsProblem}
\end{center}
\end{figure}

However, while the windows appear different when treated as atomic objects, they have many sub-regions (namely, pieces of the sine waves) that are similar when considered in isolation (Fig \ref{fig:lengthsProblem}c). This suggests that if we were to compare the windows based on \textit{local} characteristics, instead of their \textit{global} shape, we could search at a length longer than the event and still determine that windows containing event instances were similar.

To enable this, we transform the data into a sparse binary feature matrix that encodes the presence of particular shapes at each position in the time series (Fig \ref{fig:basicTransform}). Columns of the feature matrix are shown at a coarse granularity for visual clarity--in reality, there is one column per time step. We defer explanation of how these shapes are selected and how this feature matrix is constructed to the next section.
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/basicTransform}
	\caption{Feature matrix. Each row is the presence of a particular shape, and each column is a particular time (shown at reduced granularity). Similar regions of data contain the same shapes in roughly the same temporal arrangement.}
	\label{fig:basicTransform}
\end{center}
\end{figure}

Using this feature matrix, we can compare windows of data without knowing the lengths of instances. This is because, even if there is extraneous data at the ends of the windows, there will still be more common features where the event happens (Fig \ref{fig:windowTooLong}a) than would be expected by chance.

Once we identify the windows containing instances, we can recover the starts and ends of the instances by examining which columns in the corresponding windows look sufficiently \\ similar---if a start or end column does not contain a consistent set of 1s across these windows, it is probably not part of the event, and we prune it (Fig \ref{fig:windowTooLong}b).
% \begin{figure}[h]
% \begin{center}
% 	\includegraphics[width=\linewidth]{paper/windowTooLong}
% 	\caption{Because the values in the feature matrix are independent of the window length, a window longer than the event can be used to search for instances.}
% 	\label{fig:windowTooLong}
% \end{center}
% \end{figure}

Unfortunately, this figure is optimistic about the regularity of shapes within instances. In reality, a given shape will not necessarily be present in all instances, and a set of shapes may not appear in precisely the same temporal arrangement more than once because of uniform scaling \cite{keoghUSMotifs} and time warping. We defer treatment of the first point to a later section, but the second can be remedied with a preprocessing step.
\vspace{-.5mm}
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/windowTooLong}
	\caption{Because the values in the feature matrix are independent of the window length, a window longer than the event can be used to search for instances.}
	\label{fig:windowTooLong}
\end{center}
\end{figure}

To handle both uniform scaling and time warping simultaneously, we ``blur'' the feature matrix in time. The effect is that a given shape is counted as being present over an interval, rather than at a single time step. This is shown in Figure \ref{fig:blur}, using the intersection of the features in two windows as a simplified illustration of how similar they are. Since the blurred features are no longer binary, we depict the ``intersection'' as the elementwise minimum of the windows.
\vspace{-3mm}
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/blur}
	\caption{Blurring the feature matrix. Despite the second sine wave being longer and warped in time, the two windows still appear similar when blurred.}
	\label{fig:blur}
\end{center}
\end{figure}

% ------------------------------------------------
% \vspace{-3mm}
% \vspace{-1mm}
\vspace{2mm}
\subsection{Dealing with Irrelevant Features}
\vspace{-1mm}
% ------------------------------------------------

Thus far, we have assumed that the shapes encoded in the matrix are all characteristic of the event. In reality, we do not know ahead of time which shapes are relevant, and so there will also be many irrelevant features.

Fortunately, the combination of sparsity and our ``intersection'' operation causes us to ignore these extra features (Fig~\ref{fig:irrelevantFeatures}). To see this, suppose that the probability of an irrelevant feature being present at a particular location in an instance-containing window is $p_0$. Then the probability of it being present by chance in $k$ windows is $\approx p_0^k$. Feature matrices for real-world data are over $90$\% zeros, since a given subsequence can only resemble a few shapes. Consequently, $p_0$ is small (e.g., $0.05$), and $p_0^k \approx 0$ even for small $k$.
% \vspace{-1mm}
% \begin{figure}[h]
% \begin{figure}[t]
% \begin{center}
% 	\includegraphics[width=\linewidth]{paper/irrelevantFeatures}
% 	\caption{Most irrelevant features are ignored after only two or three examples, since it is unlikely for them to repeatedly be in the same place in the window. A few ``false positives'' may remain.}
% 	\label{fig:irrelevantFeatures}
% \end{center}
% \end{figure}

% ------------------------------------------------
\vspace{-1mm}
\subsection{Multiple Dimensions}
\vspace{-1mm}
% ------------------------------------------------

The generalization to multiple dimensions is straightforward: we construct a feature matrix for each dimension and concatenate them row-wise. That is, we take the union of the features from each dimension. A dimension may not be relevant, but this just means that it will add irrelevant features. Thanks to the aforementioned combination of sparsity and the intersection operation, we ignore these features with high probability.
% \begin{figure}[h]
% \begin{center}
% 	\includegraphics[width=\linewidth]{paper/irrelevantFeatures}
% 	\caption{Most irrelevant features are ignored after only two or three examples, since it is unlikely for them to repeatedly be in the same place in the window. A few ``false positives'' may remain.}
% 	\label{fig:irrelevantFeatures}
% \end{center}
% \end{figure}
% \begin{figure}[t]
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/irrelevantFeatures}
	\caption{Most irrelevant features are ignored after only two or three examples, since it is unlikely for them to repeatedly be in the same place in the window. A few ``false positives'' may remain.}
	\label{fig:irrelevantFeatures}
\end{center}
\end{figure}

% ------------------------------------------------
% \vspace{-1mm}
\subsection{Finding Instances} \label{sec:basicAlgo}
% ------------------------------------------------
The previous subsections have described how we construct the feature matrix. In this section, we describe how to use this matrix to find event instances. A summary is given in Algorithm~\ref{algo:findInstancesShort}. The idea is that if we are given one ``seed'' window that contains an instance, we can generate a set of similar ``candidate'' windows and then determine which of these are likely event instances. Since we cannot generate seeds that are certain to contain instances, we generate many seeds and try each. We defer explanation of how seeds are generated to the next section.

\begin{algorithm}[h]
\caption{$FindInstances(\mathcal{S}, \mathcal{X})$ }
\label{algo:findInstancesShort}
\begin{algorithmic}[1]

\State \textbf{Input:} $S$, ``seed'' windows; $\mathcal{X}$, all blurred windows

\State $\mathcal{I}_{best} \leftarrow \{\}$
\State $score_{best} \leftarrow -\infty$
\For {each seed window $s \in \mathcal{S}$}
	\LineComment{Find candidate windows $\mathcal{C}$ based on}
	\LineComment{dot product with seed window $s$}
	\State $P \leftarrow [s \cdot \tilde{x}_i, \tilde{x}_i \in \mathcal{X}]$
	\State $\mathcal{C} \leftarrow localMaxima(P)$
	\State $\mathcal{C} \leftarrow enforceMinimumSpacing(\mathcal{C}, M_{min})$
	\State $\mathcal{C} \leftarrow sortByDescendingDotProd(\mathcal{C}, P)$
	\LineComment{assess subsets of $\mathcal{C}$}
	\For {$k \leftarrow 2,\ldots,|\mathcal{C}|$}
		\State $\mathcal{I} \leftarrow \{c_1,...,c_k\}$ \COMMENT{$k$ best candidates}
		\State $score \leftarrow computeScore(\mathcal{I}, c_{k+1})$
		\If { $score > score_{best}$ }
			\State $score_{best} \leftarrow score$
			\State $\mathcal{I}_{best} \leftarrow \mathcal{I}$
		\EndIf
	\EndFor
\EndFor
\Return $\mathcal{I}_{best}$
\vspace{-1mm}
\end{algorithmic}
\end{algorithm}

The main loop iterates through all seeds $s$ and generates sets of candidate windows for each. These candidates are the windows whose dot products with $s$ are local maxima---i.e., they are higher than those of the windows just before and after. To prevent excess overlap, a minimum spacing is enforced between the candidates by only taking the best relative maximum in any interval of width $M_{min}$ (the instance length lower bound). If $s$ contains an event instance, the resulting candidates should be (and typically are) a superset of the true instance-containing windows.

In the inner loop, we assess subsets of the candidates to determine which ones contain instances. Since there are $2^{|\mathcal{C}|} = O(2^{N/M_{min}})$ possible subsets, we use a greedy approach that tries only $|\mathcal{C}| = O(N/M_{min})$ subsets. Specifically, we rank the candidates based on their dot products with $s$ and assess subsets that contain the $k$ highest-ranking candidates for each possible $k$.

The final set returned is the highest-scoring subset of candidates for any seed. See Section~\ref{sec:scoring} for an explanation of the scoring function.
