
We implemented our algorithm, along with baselines from the literature \cite{moen, plmd}, using SciPy \cite{scipy}. For the baselines, we JIT-compiled the inner loops using Numba \cite{numba}. All code and raw results are publicly available at \cite{extractWebsite}. Our full algorithm, including feature matrix construction, is under 300 lines of code. To our knowledge, our experiments use more ground truth event instances than any similar work.

% ------------------------------------------------
\subsection{Datasets}
% ------------------------------------------------

We used the following datasets (Fig \ref{fig:datasets}), selected on the basis that they were both publicly available and contained repeated instances of some ground truth event, such as a repeated gesture or spoken word.

Some of these events could be isolated with simpler techniques than those considered here---e.g., an appropriately-tuned edge detector could find many of the instances in the TIDIGITS time series. However, \textit{the goal of our work is to find events \textbf{without} requiring users to design features and algorithms for each domain or event of interest}. Thus, we deliberately refrain from exploiting dataset-specific features or prior knowledge. Moreover, such knowledge is rarely sufficient to solve the problem---even when one knows that events are periodic, contain peaks, etc., isolating their starts and ends programmatically is still challenging. % in our experience. % Moreover, if one did want to incorporate such information, one could easily do so by adding useful features or removing irrelevant signals for our algorithm.  % Moreover, our experience shows that even events that are obvious to the human eye are rarely easy to segment algorithmically. This means that it would likely be time-consuming to develop an alternative to our approach even for datasets that appear to be straightforward. % Even slight deviations in event length, for example, can drastically reduce autocorrelations. % Given that even detecting the period of a quasi-periodic signal can be challenging \cite{linMotifPeriodicity}, it is no surprising that precisely isolating repeating but not necessarily periodic events is difficult.

To aid reproducibility, we supplement the source code with full descriptions of our preprocessing, random seeds, etc., at \cite{extractWebsite}, and omit the details here for brevity.
\begin{figure}[h]
\begin{center}
	% \includegraphics[width=\linewidth]{../figs/paper/datasets}
	\includegraphics[width=\linewidth]{datasets}
	\caption{Example time series from each of the datasets used.}
	\label{fig:datasets}
\end{center}
\end{figure}

\subsubsection{MSRC-12} \label{sec:msrc}
The MSRC-12 dataset \cite{msrc12} consists of (x,y,z) human joint positions captured by a Microsoft Kinect while subjects repeatedly performed specific motions. Each of the 594 time series in the dataset is 80 dimensional and contains 8-12 event instances.

Each instance is labeled with a single marked time step, rather than with its boundaries, so we use the number of marks in each time series as ground truth. That is, if there are $k$ marks, we treat the first $k$ regions returned as correct. This is a less stringent criterion than on other datasets, but favors existing algorithms insofar as they often fail to identify exact event boundaries.

\subsubsection{TIDIGITS}
The TIDIGITS dataset \cite{tidigits} is a large collection of human utterances of decimal digits. We use a subset of the data consisting of all recordings containing only one type of digit (e.g., only ``9''s). We randomly concatenated sets of 5-8 of these recordings to form 1604 longer recordings in which multiple speakers utter the same word. As is standard practice \cite{minnenSubseqDensity}, we represented the resulting audio using Mel-Frequency Cepstral Coefficients (MFCCs) \cite{librosa}, rather than as the raw speech signal. Unlike in the other datasets, little background noise and few transient phenomena are present to elicit false positives; however, the need to generalize across speakers and rates of speech makes avoiding false negatives difficult. % The primary challenge in this dataset is generalizing across multiple speakers and rates of speech.

\subsubsection{Dishwasher}
The Dishwasher dataset \cite{ampds} consists of energy consumption and related electricity metrics at a power meter connected to a residential dishwasher. It contains twelve variables and two years worth of data sampled once per minute, for a total of 12.6 million data points.

We manually plotted, annotated, and verified event instances across all 1 million+ of its samples.\footnote{See our supporting website \cite{extractWebsite} for details.}

Because this data is 100x longer than what the comparison algorithms can process in a day \cite{plmd}, we followed much the same procedure as for the TIDIGITS dataset. Namely, we extracted sets of 5-8 event instances, along with the data around them (sometimes containing other transient phenomena), and concatenated them to form shorter time series.

\subsubsection{UCR}
Following \cite{plmd}, we constructed synthetic datasets by planting examples from the UCR Time Series Archive \cite{ucrTimeSeries} in random walks. We took examples from the 20 smallest datasets (before the 2015 update), as measured by the lengths of their examples. For each dataset, we created 50 time series, each containing five examples of one class. This yields 1000 time series and 5000 instances.

% Note that this is two orders of magnitude larger than the subset of datasets and instances used in \cite{plmd}, which may explain why our results using their algorithm are not as optimistic as their own.

% ------------------------------------------------
% \vspace{1mm}
\subsection{Evaluation Measures}
% ------------------------------------------------
Let $\mathcal{R}$ be the ground truth set of instance regions and let $\mathcal{\hat{R}}$ be the set of regions returned by the algorithm being evaluated. Further let $r_1 = (a_1, b_1)$ and $r_2 = (a_2, b_2)$ be two regions.
\begin{Definition}{$IOU(r_1, r_2)$. The \textbf{Intersection-Over-Union (IOU)} of $r_1$ and $r_2$ is given by $|r_1 \cap r_2| / |r_1 \cup r_2|$, where $r_1$ and $r_2$ are treated as intervals.}
\end{Definition}

\vspace{-3mm}
\begin{Definition}{$Match(r_1, r_2, \tau)$. $r_1$ and $r_2$ are said to \textbf{Match} at a threshold of $\tau$ iff $IOU(r_1, r_2) \ge \tau$.
}
\end{Definition}
\vspace{-3mm}
\begin{Definition}{MatchCount$(\mathcal{\hat{R}}, \mathcal{R}, \tau)$. The \textbf{MatchCount} of $\mathcal{\hat{R}}$ given $\mathcal{R}$ and $\tau$ is the greatest number of matches at threshold $\tau$ that can be produced by pairing regions in $\mathcal{\hat{R}}$ with regions in $\mathcal{R}$ such that no region in either set is present in more than one pair.\footnote{Since regions (and their possible matches) are ordered in time, this can be computed greedily after sorting $\mathcal{\hat{R}}$ and $\mathcal{R}$.}
}
\end{Definition}
% \vspace{-2mm}
\begin{Definition}{\textbf{Precision, Recall}, and \textbf{F1 Score}.
}
\end{Definition}
% \vspace{-6mm}
% \vspace*{-10mm}
\vspace*{-6mm}
\begin{align}
% \vspace*{-15mm}
	Precision(\mathcal{\hat{R}}, \mathcal{R}, \tau)
		&= MatchCount(\mathcal{\hat{R}}, \mathcal{R}, \tau) / |\mathcal{\hat{R}}| \\
	Recall(\mathcal{\hat{R}}, \mathcal{R}, \tau)
		&= MatchCount(\mathcal{\hat{R}}, \mathcal{R}, \tau) / |\mathcal{R}| \\
	F1(\mathcal{\hat{R}}, \mathcal{R}, \tau)
		&= \frac{2 \cdot Precision \cdot Recall}{Precision + Recall}
\end{align}

% To evaluate accuracy, we use $IOU(\cdot,\cdot)$, $F1_{.5}(\cdot,\cdot)$ and $F1_{.25}(\cdot,\cdot)$. To evaluate running time, we use seconds of CPU time. All baseline algorithms use the same code for their inner loops, so this measure is robust to implementation bias.

% ------------------------------------------------
\subsection{Comparison Algorithms}
% ------------------------------------------------

While none of the techniques we reviewed both seek to solve our problem and operate under assumptions as relaxed as ours, we found that two existing algorithms solving the univariate version of the problem could be generalized to the multivariate case:
\begin{enumerate}
\itemsep.1em
	\item Finding the closest pair of subsequences under the z-normalized Euclidean distance, and returning as instances all subsequences within some threshold distance of this pair \cite{moen, minnenSubdim}. In our case, distance is defined as the sum of the distances for each dimension, normalized individually. We find the closest pair efficiently using the MK algorithm \cite{mk} plus the length-pruning technique of Mueen \cite{moen}. We determine the distance threshold using Minnen's algorithm \cite{minnenNeighborhood}. We call this algorithm \textit{Dist}.
	\item The single-motif-finding subroutine of \cite{plmd}, with distances and description lengths summed over dimensions. This amounts to closest-pair motif discovery to find seeds, candidate generation based on Euclidean distance to these seeds, and instance selection using a Minimum Description Length (MDL) criterion.\footnote{In the case of a single event type, this maximizes the same objective as \cite{epenthesis}, but requires fewer closest-pair searches. We therefore compare only to the subroutine of \cite{plmd}.} We call this algorithm \textit{MDL}.
\end{enumerate}
%  generalizes by taking two instances at a time, rather than seeking all of them, but we do not consider this variation since it is slower and maximizes the same objective in the case of a single event type.}

In both cases, we consider versions of the algorithms that carry out searches at lengths from $M_{min}$ to $M_{max}$ and use the best result from any length. This means lowest distance in the former case and lowest description length in the latter. In other words, we give them the prior knowledge that there is exactly one type of event to be found, as well as its approximate length. This replaces the heuristics for determining the number of event classes described in the original papers. % For both algorithms, we allow instances to overlap by $M_{min}/2$.

We tried many variations of these two algorithms regarding threshold function, description length computation, and other properties, and use the above approaches because they worked the best.

% ------------------------------------------------
\subsection{Instance Discovery Accuracy}
% ------------------------------------------------
The core problem addressed by our work is the robust location of multiple event instances within a time series known to contain a small number of them. To assess our effectiveness in solving this problem, we evaluated the F1 score on each of the four datasets, varying the threshold $\tau$ for how much ground truth and reported instances needed to overlap in order to count as matching. In all cases, $M_{min}$ and $M_{max}$ were set to $1/20$ and $1/10$ of the time series length. As shown in Figure~\ref{fig:accuracy}, we outperform the comparison algorithms for virtually all match thresholds on all datasets.
\vspace{-1mm}
\begin{figure}[h]
\begin{center}
	% \includegraphics[width=\linewidth]{paper/acc}
	\includegraphics[width=\linewidth]{acc}
	\vspace*{-6mm}
	\caption{The proposed algorithm is more accurate for virtually all ``match'' thresholds on all datasets. Shading corresponds to 95\% confidence intervals.}
	\label{fig:accuracy}
\end{center}
\end{figure}

\vspace{1mm}
Note that MSRC-12 values are constant because instance boundaries are not defined in this dataset (see Section \ref{sec:msrc}). Further, the dataset on which we perform the closest to the comparisons (UCR) is synthetic, univariate, and only contains instances that are the same length. These last two attributes are what \textit{Dist} and \textit{MDL} were designed for, so the similar F1 scores suggest that \textit{EXTRACT}'s superiority on other datasets is due to its robustness to violation of these conditions. Visual examination of the errors on this dataset suggests that all algorithms have only modest accuracy because there are often regions of random walk data that are more similar in shape to one another than the instances are.

The dropoffs in Figure~\ref{fig:accuracy} at particular IOU thresholds indicate the typical amount of overlap between reported and true instances. E.g., the fact that existing algorithms abruptly decrease in F1 score on the TIDIGITS dataset at a threshold near 0.3 suggests that many of their reported instances only overlap this much with the true instances.

\vspace{-.5mm}
Our accuracy on real data is not only superior to the comparisons, but also high in absolute terms (Table~1). Suppose that we consider IOU thresholds of 0.25 or 0.5 to be ``correct'' for our application. The former might correspond to detecting a portion of a gesture, and the latter might correspond to detecting most of it, with a bit of extra data at one end. At each of these thresholds, our algorithm discovers event instances with an F1 score of over $.9$ on real data.
% \vspace{1.5em}
\vspace{5mm}
\begin{table}[h]
\setlength\tabcolsep{4pt} % default value: 6pt
\centering
\caption*{Table 1: EXTRACT F1 Scores are High in Absolute Terms}
\label{tbl:f1}
\begin{tabularx}{\linewidth}{@{\hskip-1.5pt}llll|lll}
% \setlength\tabcolsep{1mm}
% \hline
% {}
\hline
& \multicolumn{3}{c}{Overlap $\ge .25$} & \multicolumn{3}{c}{Overlap $\ge .5$} \\
& Ours &   MDL & Dist & Ours &  MDL & Dist \\
\hline
Dishwasher & \textbf{0.935} & 0.786 &  0.808 & \textbf{0.910} & 0.091 &  0.191 \\
TIDIGITS   & \textbf{0.955} & 0.779 &  0.670 & \textbf{0.915} & 0.140 &  0.174 \\
MSRC-12    & \textbf{0.947} & 0.943 &  0.714 & \textbf{0.947} & 0.943 &  0.714 \\
UCR        & \textbf{0.671} & 0.593 &  0.587 & \textbf{0.539} & 0.510 &  0.504 \\
\hline
\end{tabularx}
\end{table}
\vspace{-1mm}

An example of our algorithm's output on the TIDIGITS dataset is shown in Figure~\ref{fig:algoOutput}. The regions returned (shaded) closely bracket the individual utterances of the digit ``0.'' The ``Learned Pattern'' is the feature weights $V$ from Section~\ref{sec:instanceBounds}, which are the increases in log probability of each element being 1 when the window is an event instance.
% \begin{figure}[h]
\begin{figure}[t]
\begin{center}
	% \includegraphics[width=\linewidth]{paper/tidigits-output}
	\includegraphics[width=\linewidth]{tidigits-output}
	\vspace*{-6mm}
	\caption{\textit{Top}) Original time series, with instances inferred by EXTRACT in gray. \textit{Bottom}) The feature matrix $\Phi$. \textit{Right}) The learned feature weights. These resemble a ``blurred'' version of the features that occur when the word is spoken.}
	% \caption{EXTRACT output on a TIDIGITS time series. \textit{Top}) Original time series. \textit{Bottom}) The feature matrix $\Phi$. \textit{Right}) The learned feature weights. These resemble a ``blurred'' version of the features that repeat each time the word is spoken.}
	\label{fig:algoOutput}
\end{center}
\end{figure}

% ------------------------------------------------
% \vspace{-2mm}
\subsection{Speed}
% ------------------------------------------------
In addition to being accurate on both real and synthetic data, our algorithm is also fast. To assess performance, we recorded the time it and the comparisons took to run on increasingly long sections of random walk data and the raw Dishwasher data.

In the first column of Fig \ref{fig:scalability}, we vary only the length of the time series ($N$) and keep $M_{min}$ and $M_{max}$ fixed at 100 and 150. In the second column, we hold $N$ constant at 5000 and vary $M_{max}$, with $M_{min}$ fixed at $M_{max} - 50$ so that the number of lengths searched is constant. In the third column, we fix $N$ at 5000 and set ($M_{min}$, $M_{max}$) to $(150,150),(140,160),$\ldots$,(100,200)$.
\begin{figure}[h]
\begin{center}
	% \includegraphics[width=\linewidth]{paper/scalability/scalability}
	\includegraphics[width=\linewidth]{scalability}
	\caption{The proposed algorithm is one to two orders of magnitude faster than comparisons.}
	\label{fig:scalability}
\end{center}
\end{figure}

Our algorithm is at least an order of magnitude faster in virtually all experimental conditions. Further, it shows little or no increase in runtime as $M_{min}$ and $M_{max}$ are varied and increases only slowly with $N$. This is in line with what would be expected given our computational complexity, except with even less dependence on $M_{max}$. This deviation is because $D\log(M_{max})\log(N)$ is an upper bound on the number of features used---the actual number is lower since shapes that only occur once are discarded. This is also why our algorithm is faster on the Random Walk dataset than the Dishwasher dataset; random walks have few repeating shapes, so our feature matrix has few rows.

Both \textit{Dist} and \textit{MDL} sometimes plateau in runtime thanks to their early-abandoning techniques. \textit{Dist} even decreases because the lower bound it employs \cite{moen} to prune similarity comparisons is tighter for longer time series. Since they are $O(N^2)$, they are also helped by the  decrease in the number of windows to check as the maximum window length $M_{max}$ increases. This decrease benefits EXTRACT as well, but to a lesser extent since it is subquadratic.

As with accuracy, our technique is fast not only relative to comparisons, but also in absolute terms---we are able to run the above experiments in minutes and search each time series in seconds (even with our simple Python implementation). Since these time series reflect phenomena spanning many seconds or hours, this means that our algorithm could be run in real time in many settings.

