
% We have carried out what we believe is the most rigorous set of time series motif discovery experiments ever conducted. Further, all of our code and raw results are publicly available \cite{flockWebsite}, and we have endeavored to make our data cleaning code particularly simple and modular in the hopes that it will be of use to future researchers.
%Below, we discuss the metrics we used to evaluate our algorithm, the baselines against which we compared, the datasets used, and each of our experiments.

We implemented our algorithm, along with baselines from the literature \cite{moen, plmd}, using SciPy \cite{scipy}. For the baselines, we JIT-compiled the inner loops using Numba \cite{numba}. All code and raw results are publicly available at \cite{flockWebsite}. We have endeavored to make our data cleaning code particularly simple and modular in the hopes that it will be useful to future researchers. Our full algorithm, including feature matrix construction, is under 300 lines of code.

% ------------------------------------------------
\subsection{Datasets}
% ------------------------------------------------

We used the following datasets (Fig \ref{fig:datasets}), selected on the basis that they were both publicly available and contained repeated instances of some ground truth pattern, such as a repeated gesture or spoken word.
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{../figs/paper/datasets}
	\caption{Example time series from each of the datasets used.}
	\label{fig:datasets}
\end{center}
\end{figure}

Some of these datasets could be mined with simpler techniques than those considered here---e.g., an appropriately-tuned peak detector could find many of the instances in the TIDIGITS time series. However, the goal of our work is to find patterns \textit{without} the knowledge that they resemble peaks, tend to be close together, etc. Thus, we allow these simplifying characteristics to be present, but refrain from exploiting them.

To aid reproducibility, we supplement the source code with full descriptions of our preprocessing, randomization, etc, at \cite{flockWebsite}, and omit the details here for brevity.

% Each instance is annotated with a single time step to mark a point during which it was taking place. These annotations tend to be near the middles of pattern instances, but this is inconsistent. We initially used their placement in our evaluations, requiring reported instances to contain exactly one annotation, but found that this added noise to the assessment--the edge cases wherein reported instances happened to exactly fall between annotations dominated the errors of all algorithms evaluated. Consequently, we simply use the number of annotations in a given time series to determine the number of instances that should be reported, and assume that all instances up to this count are positioned properly. Note that this is a boon to the baseline algorithms, which often fail to identify instance starts and ends properly.

\subsubsection{TIDIGITS}
The TIDIGITS dataset \cite{tidigits} is a large collection of human utterances of decimal digits. We use a subset of the data consisting of all recordings containing only one type of digit (e.g., only ``9''s). We randomly concatenated sets of 5-8 of these recordings to form 1604 time series in which multiple speakers utter the same word. As is standard practice \cite{minnenSubseqDensity}, we represented the resulting time series using Mel-Frequency Cepstral Coefficients (MFCCs), rather than as the raw speech signal.

% Using librosa [], we extracted the first 13 Mel-Frequency Cepstral Coefficients (MFCCs) with a sliding window of length 10ms, stride of 5ms, and FFT length of 512. We then downsampled this densely-sampled data by a factor of 2. Finally, we shuffled these recordings and concatenated them into longer time series containing five to eight digit instances each. The resulting time series consist of slightly more silence than speech, with pauses ranging from a second or more to a few milliseconds. In total we have \%d time series, containing \%d total coefficients and \%d instances.

% Because each recording brackets the utterance with silence, we used as instance boundaries the first and last time steps in which the signal power exceeded 10\% of its maximum value. This tends to be within a 5-10\% of the correct boundaries.

\subsubsection{Dishwasher}
The Dishwasher dataset \cite{ampds} consists of energy consumption and related electricity metrics at a power meter connected to a residential dishwasher. It contains twelve variables and two years worth of data sampled once per minute, for a total of 12.6 million data points.

%Crucially, the patterns in this dataset are easily recognizable to a human annotator, including their precise starts and ends. Leveraging this,
We manually plotted, annotated, and verified pattern instances in all 1 million+ of its subsequences.\footnote{See our supporting website \cite{flockWebsite} for details.}

Because this data is 100x longer than what the comparison algorithms can process in a day \cite{plmd}, we followed much the same procedure as for the TIDIGITS dataset. Namely, we extracted sets of five to eight pattern instances and concatenated them to form shorter time series.

% It was not possible to run the baseline algorithms on this raw data since it was too long (indeed, even the optimized implementation of [] present in the original paper took most of a day to mine a length 10,000 time series, and this is 100x larger). Thus, we followed much the same procedure as for the TIDIGITS dataset, extracting sets of five pattern instances and concatenating them to form shorter time series. We also extracted a random amount of data around each instance, with length in [25, 200] on either side (the patterns are of length $\sim$150).

\subsubsection{MSRC-12} \label{sec:msrc}
The MSRC-12 dataset \cite{msrc12} consists of (x,y,z) human joint positions captured by a Microsoft Kinect while subjects repeatedly performed particular motions. Each of the 594 time series in the dataset is 80 dimensional and contains 8-12 pattern instances.

% Each instance $(a, b)$ is labeled with a single marked time step $t, a < t < b$, rather than with the boundaries $a$ and $b$ themselves
Each instance is labeled with a single marked time step, rather than with its boundaries, so we use the number of marks in each time series as ground truth. That is, if there are $k$ marks, we treat the first $k$ regions returned as correct. This is a less stringent criterion than on other datasets, but favors existing algorithms insofar as they often fail to identify exact region boundaries.
% Since this does not provide us with region boundaries (and we found that it was unsafe to assume that they were, e.g., in the middle of each region)

\subsubsection{UCR}
Following \cite{plmd}, we constructed synthetic datasets by planting examples from the UCR Time Series Archive \cite{ucrTimeSeries} in random walks. We took examples from the 20 smallest datasets, as measured by the lengths of their objects. For each dataset, we created 50 time series, each containing five objects of one class. This yields 1000 time series and 5000 instances.

Note that this is two orders of magnitude larger than the subset of datasets and instances used in \cite{plmd}, which may explain why our results using their algorithm are not as optimistic as their own.

% ------------------------------------------------
\vspace{1mm}
\subsection{Evaluation Measures}
% ------------------------------------------------
Let $\mathcal{R}$ be the ground truth set of instance regions and let $\mathcal{\hat{R}}$ be the set of regions returned by the algorithm being evaluated. Further let $r_1 = (a_1, b_1)$ and $r_2 = (a_2, b_2)$ be two regions.
\begin{Definition}{$IOU(r_1, r_2)$. The \textbf{Intersection-Over-Union (IOU)} of $r_1$ and $r_2$ is given by $|r_1 \cap r_2| / |r_1 \cup r_2|$, where $r_1$ and $r_2$ are treated as intervals.}
\end{Definition}
% Let $s_1$ and $s_2$ be the sets $\{a_1,\ldots,b_1\} and \{a_2,\ldots,b_2\}.
% |s_1 \cap s_2| / |s_1 \cap s_2|$
% \begin{Definition}{$IOU(r_1, r_2)$. The \textbf{Intersection-Over-Union (IOU)} of $r_1$ and $r_2$ is given by the number of indices $i$ within both $r_1$ and $r_2$ divided by the number of indices in either $r_1$ or $r_2$.}
% \end{Definition}
\vspace{-4mm}
\begin{Definition}{$Match(r_1, r_2)$. $r_1$ and $r_2$ are said to \textbf{Match} at a threshold of $\tau$ iff $IOU(r_1, r_2) \ge \tau$.
}
\end{Definition}
\vspace{-4mm}
\begin{Definition}{MatchCount$(\mathcal{\hat{R}}, \mathcal{R}, \tau)$. The \\ \textbf{MatchCount} of $\mathcal{\hat{R}}$ given $\mathcal{R}$ and $\tau$ is the greatest number of matches at threshold $\tau$ that can be produced by pairing regions in $\mathcal{\hat{R}}$ with regions in $\mathcal{R}$ such that no region in either set is present in more than one pair.\footnote{Since regions (and their possible matches) are ordered in time, this can be computed greedily after sorting $\mathcal{\hat{R}}$ and $\mathcal{R}$.}
}
\end{Definition}
% \vspace{-2mm}
\begin{Definition}{\textbf{Precision, Recall}, and \textbf{F1 Score}.
}
\end{Definition}
\vspace{-4mm}
\begin{align}
	Precision(\mathcal{\hat{R}}, \mathcal{R}, \tau)
		&= MatchCount(\mathcal{\hat{R}}, \mathcal{R}, \tau) / |\mathcal{\hat{R}}| \\
	Recall(\mathcal{\hat{R}}, \mathcal{R}, \tau)
		&= MatchCount(\mathcal{\hat{R}}, \mathcal{R}, \tau) / |\mathcal{R}| \\
	F1(\mathcal{\hat{R}}, \mathcal{R}, \tau)
		&= \frac{2 \cdot Precision \cdot Recall}{Precision + Recall}
\end{align}

% To evaluate accuracy, we use $IOU(\cdot,\cdot)$, $F1_{.5}(\cdot,\cdot)$ and $F1_{.25}(\cdot,\cdot)$. To evaluate running time, we use seconds of CPU time. All baseline algorithms use the same code for their inner loops, so this measure is robust to implementation bias.

% ------------------------------------------------
\subsection{Comparison Algorithms}
% ------------------------------------------------

% Although we reviewed over 70 papers in our search for a solution to this problem, we found that only a few basic techniques have been proposed. This is partially because many works focus on accelerating existing algorithms [] or modifying them to work with slightly different distance measures [], and many simply assume the efficacy of the core pattern discovery techniques and focus their contributions elsewhere [].

% Further, as discussed in the Related Work, many of the basic techniques that have been employed do not have clear generalizations to multivariate time series. Moreover, numerous works' contributions center on refinements to the set of patterns discovered [], which depend on accurately finding instances of each pattern in the first place. Thus, since we focus our investigation on finding these patterns, we find that there are only two approaches that merit formal evaluation:

% Many algorithms have been proposed for time series pattern discovery. However, as discussed in the related work, few consider the problem of finding all pattern instances (and fewer still demonstrate that their algorithms do this on more than a single real-world dataset or domain). Further, many works require univariate data or focus on enhancements that assume discovery of single patterns as a subroutine---e.g., post-hoc refinement of patterns, speedup techniques, or finding multiple patterns---and are thus orthogonal to our contribution.

% Consequently, we found only two baseline algorithms:

% shorter version of above: inclusion criteria = tested on > 1 dataset with good results and the definition of a "match" given, no pipelines/refinement steps (cuz could throw those at anything), works on multidimensional stuff, no specializing assumptions, such as a particular problem domain, dataset, or pattern property (in particular, the assumption that all instances have identical means and amplitudes and so z-normalization is unnecessary), no quadratic or worse runtimes in N (unless much faster in practice)
% also just emphasize that closest pair tends to dominate the literature and these are the 2 ways to generalize it for a single pattern

While none of the 70+ techniques we reviewed operate under assumptions as relaxed as ours, we found that two existing algorithms solving the univariate version of the problem could be generalized to the multivariate case:
\begin{enumerate}
\itemsep-.1em
	\item Closest-pair motif discovery to find seed windows, followed by instance selection using a distance threshold \cite{moen, minnenSubdim}. Distance is defined as the sum of the z-normalized Euclidean distances of each dimension. We find this pair efficiently using the MK algorithm \cite{mk} plus the length-pruning technique of Mueen \cite{moen}. We determine the threshold using Minnen's algorithm \cite{minnenNeighborhood}. We call this algorithm \textit{Dist}.
	%We use the latter since it appears to be the only systematic investigation into how this threshold ought to be set and we found that it outperformed other alternatives.
	\item The algorithm of \cite{plmd}, with distances and description \\ lengths summed over dimensions. This amounts to closest-pair motif discovery to find seeds, followed by instance selection using a minimum description length (MDL) criterion. We call this algorithm \textit{MDL}.
	% \item Closest-pair motif discovery, followed by instance selection using the Minimum Description Length (MDL) algorithm of \cite{plmd}. We term this algorithm \textit{MDL}.
\end{enumerate}

In both cases, we consider versions of the algorithms that carry out searches at lengths from $M_{min}$ to $M_{max}$ and use the best result from any length. This means lowest distance in the former case and lowest description length in the latter. In other words, we give them the prior knowledge that there is exactly one pattern to be found, as well as its approximate length. This replaces the heuristics for determining the number of patterns described in the original papers.

We tried many variations of these two algorithms, and use the above approaches because they worked the best.

%Since it greatly reduces runtime with little or no accuracy impact, we check every fifth length, rather than all of them (despite the fact that this reduces the performance advantage of our own algorithm).

% When computing distances between subsequences in these algorithms, we z-normalize within each dimension separately and sum the squared Euclidean distances across dimensions to get the total distance. We then divide by the lengths of the subsequences being considered when comparing distances across lengths.

% For both algorithms, we allowed 25\% overlap of instances, since a no-overlap constraint greatly reduced accuracy where instances were densely packed together.

% We had to make a modification to the MDL algorithm for it to be effective, so we do not refer to it by its original name (Proper Length Motif Discovery) to avoid confusion. Namely, we did not early abandon the search over lengths when the closest pair at a given length had a negative bitsave---since this was \textit{usually} the case on our real-world data---but instead early abandoned when the best bitsave using any number of instances at a length was negative. Both methods are heuristics for when to stop searching longer lengths; our modification simply prevents it from stopping far too early.

% ------------------------------------------------
\subsection{Instance Discovery Accuracy}
% ------------------------------------------------
The core problem addressed by our work is the robust location of multiple pattern instances within an arbitrary time series. To assess our effectiveness in solving this problem, we evaluated the F1 score on each of the four datasets, varying the threshold $\tau$ for how much ground truth and reported instances needed to overlap in order to count as matching. As shown in Figure~\ref{fig:accuracy}, we outperform the comparison algorithms for virtually all match thresholds on all datasets. In all cases, $M_{min}$ and $M_{max}$ were set to $1/20$ and $1/10$ of the time series length.
% For all datasets but MSRC-12, $M_{min}$ and $M_{max}$ were set to $1/20$ and $1/10$ of the time series length. For MSRC-12, in which instances were a smaller frac they were $1/16$ and $1/8$, since
\vspace{-1mm}
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/acc}
	\vspace*{-6mm}
	\caption{The proposed algorithm is more accurate for virtually all ``match'' thresholds on all datasets. Shading corresponds to 95\% confidence intervals.}
	\label{fig:accuracy}
\end{center}
\end{figure}

Note that MSRC-12 values are constant because region boundaries are not defined in this dataset (see \ref{sec:msrc}). Further, the dataset on which we perform the closest to the comparisons (UCR) is synthetic, univariate, and only contains instances that are the same length. These last two attributes are what \textit{Dist} and \textit{MDL} were designed for, so the similar F1 scores suggest that \textit{Flock}'s superiority on other datasets is due to its robustness to violation of these conditions. Visual examination of the errors on this dataset suggests that all algorithms have only modest accuracy because there are often regions of random walk data that are more similar in shape than the instances are.

The dropoffs in Figure~\ref{fig:accuracy} at particular IOU thresholds indicate the typical amount of overlap between reported and true instances. E.g., the fact that existing algorithms abruptly decrease in F1 score on the TIDIGITS dataset at a threshold near 0.3 suggests that many of their reported instances only overlap this much with the true instances.

Our accuracy on real data is not only superior to the comparisons, but also high in absolute terms (Table~\ref{tbl:f1}). Suppose that we consider IOU thresholds of 0.25 or 0.5 to be ``correct'' for our application. The former might correspond to detecting a portion of a gesture, and the latter might correspond to detecting most of it, with a bit of extra data at one end. At these thresholds, our algorithm discovers pattern instances with an F1 score of over 90\% on real data.
% \vspace{1.5em}
\vspace{1mm}
\begin{table}[h]
\setlength\tabcolsep{4pt} % default value: 6pt
\centering
\caption{Flock F1 Scores are High in Absolute Terms} \label{tbl:f1}
\begin{tabularx}{\linewidth}{@{\hskip-1.5pt}llll|lll}
% \setlength\tabcolsep{1mm}
% \hline
% {}
\hline
& \multicolumn{3}{c}{Overlap $\ge .25$} & \multicolumn{3}{c}{Overlap $\ge .5$} \\
& Flock &   MDL & Dist & Flock &  MDL & Dist \\
\hline
Dishwasher & \textbf{0.935} & 0.786 &  0.808 & \textbf{0.910} & 0.091 &  0.191 \\
TIDIGITS   & \textbf{0.955} & 0.779 &  0.670 & \textbf{0.915} & 0.140 &  0.174 \\
MSRC-12    & \textbf{0.947} & 0.943 &  0.714 & \textbf{0.947} & 0.943 &  0.714 \\
UCR        & \textbf{0.671} & 0.593 &  0.587 & \textbf{0.539} & 0.510 &  0.504 \\
\hline
\end{tabularx}
\end{table}

An example of our algorithm's output on the TIDIGITS dataset is shown in Figure~\ref{fig:algoOutput}. The regions returned (shaded) closely bracket the individual utterances of the digit ``0.'' The ``Learned Pattern'' is the feature weights $V$ from Section~\ref{sec:instanceBounds}, which are the increases in log probability of each element being 1 when the window is a pattern instance.
\begin{figure}[h]
\begin{center}
	% \includegraphics[width=\linewidth]{paper/msrc-output}
	% \caption{Flock output on an MSRC-12 time series. Top) original time series. Bottom) The feature matrix $\Phi$. Right) The learned weights for each feature. Observe the weights are similar to a ``blurred'' version of the features that repeat each time the gesture happens. Shaded areas are the regions returned by our algorithm. TODO fig illegible}
	\includegraphics[width=\linewidth]{paper/tidigits-output}
	\vspace*{-6mm}
	\caption{Flock output on a TIDIGITS time series. \textit{Top}) Original time series. \textit{Bottom}) The feature matrix $\Phi$. \textit{Right}) The learned feature weights. These resemble a ``blurred'' version of the features that repeat each time the word is spoken.}
	\label{fig:algoOutput}
\end{center}
\end{figure}

% ------------------------------------------------
% \vspace{-2mm}
\subsection{Scalability}
% ------------------------------------------------
In addition to being more accurate on both real and synthetic data, our algorithm is also much faster. To assess this, we recorded the time it and the comparisons took to run on increasingly long sections of random walk data and the raw Dishwasher data.
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/scalability/scalability}
	\caption{The proposed algorithm is one to two orders of magnitude faster than comparisons.}
	\label{fig:scalability}
\end{center}
\end{figure}

In the first column of Fig \ref{fig:scalability}, we vary only the length of the time series ($N$) and keep $M_{min}$ and $M_{max}$ fixed at 100 and 150. In the second column, we hold $N$ constant at 5000 and vary $M_{max}$, with $M_{min}$ fixed at $M_{max} - 50$ so that the number of lengths searched is constant. In the third column, we fix $N$ at 5000 and set ($M_{min}$, $M_{max}$) to $(150,150),(140,160),$\ldots$,(100,200)$.

Our algorithm is at least an order of magnitude faster in virtually all experimental conditions. Further, it shows little or no increase in runtime as $M_{min}$ and $M_{max}$ are varied and increases only slowly with $N$. This is in line with what would be expected given our computational complexity, except with even less dependence on $M_{max}$. This deviation is because $D\log(M_{max})\log(N)$ is an upper bound on the number of features used---the actual number is lower since shapes that only occur once are discarded (c.f.~\ref{sec:featureMat}). This is also why our algorithm is faster on the Random Walk dataset than the Dishwasher dataset; random walks have few repeating shapes, so our feature matrix has few rows.

Both \textit{Dist} and \textit{MDL} sometimes plateau in runtime thanks to their early-abandoning techniques. \textit{Dist} even decreases because the lower bound it employs \cite{moen} to prune similarity comparisons is tighter for longer time series. Since they are $O(N^2)$, they are also helped by the  decrease in the number of windows to check as the maximum window length $M_{max}$ increases. This decrease benefits Flock as well, but to a lesser extent since it is subquadratic.

As with accuracy, our technique is not only better than the comparisons, but good in absolute terms---we are able to run the above experiments in minutes and search each time series in seconds (even with our simple Python implementation). Since these time series reflect phenomena spanning many seconds or hours, this means that our algorithm could be run in real time in many settings.

