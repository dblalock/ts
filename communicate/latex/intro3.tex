
The rise of wearable technology and the internet of things have made available a vast amount of sensor data, and with it the promise of improvements in everything from human health \cite{bsnSubdim} to user interfaces \cite{minnenAffixHMMs} to agriculture \cite{keoghInsect}. Unfortunately, the raw sequences of numbers comprising this data are often insufficient to offer value. For example, a smart watch user is not interested in their arm's acceleration signal, but in having their gestures or actions recognized.

Spotting such high-level phenomena using low-level signals is currently problematic. Given enough labeled examples of the phenomena taking place, one could, in principle, train a classifer for this purpose.
% Unfortunately, this suffers from two problems:
% \begin{enumerate}
% \item It assumes prior knowledge of what phenomena exist to be recognized.This may be acceptable in some use cases, but as the large body of work on time series pattern discovery suggests,
% \end{enumerate}
Unfortunately, obtaining labeled examples is an arduous task \cite{nuactiv, dataDicts, ushapelets, fastUshapelets}. While data such as images and text can be culled at scale from the internet, most time series data cannot. Furthermore, the uninterpretability of raw sequences of numbers often makes time series difficult or impossible for humans to annotate \cite{nuactiv}. Perhaps worst of all, one may not even know what phenomena exist to be labeled---indeed, much of the literature on mining time series focuses on pattern \textit{discovery} \cite{mk,dame,plmd,rareMotif,bsnSubdim,motifOrig,minnenSubdim,minnenSubseqDensity, keoghUSMotifs}. Since human labeling is expensive or impossible, what is needed is an unsupervised, \textit{algorithmic} method of discovering and localizing patterns in raw data \cite{minnenAffixHMMs, neverEnding}.
% Even directly recording examples of a phenomenon is challenging \cite{dataDicts, ushapelets, fastUshapelets}; for example, to obtain reliable human action recordings, Ratanamahatana \& Keogh \cite{gunPointOrig} went as far as having an actor move according to a metronome.

% In this paper, we describe an algorithm that quickly and accurately achieves this.
In this paper, we describe such an algorithm. Specifically, given a multivariate time series in which certain time intervals contain a characteristic but unknown pattern, our algorithm returns these intervals and a learned model of the pattern.

As an illustration of our problem, consider Figure~\ref{fig:fig1}. The various lines depict the (normalized) current, voltage, and other power measures of a home dishwasher. Shown are three instances of the dishwasher running, with idleness in between. With no prior knowledge or domain-specific tuning, our algorithm correctly determines not only what this repeating pattern looks like, but also where it begins and ends.
\vspace{-.5mm}
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/fig1}
	% \caption{\textit{a}) True instances of the dishwasher running (shaded). \textit{b}) Even when told the length and number of pattern instances, the recent algorithms of \cite{moen} (shown) and \cite{plmd} (almost identical) return intervals with only the beginning of the pattern. \textit{c}) Our proposed algorithm returns accurate intervals with no such prior knowledge.}
	\vspace*{-4.5mm}
	\caption{\textit{a}) True instances of the dishwasher running (shaded). \textit{b}) Even when told the length and number of pattern instances, the recent algorithm of \cite{plmd} returns intervals with only the beginning of the pattern. \textit{c}) Our proposed algorithm returns accurate intervals with no such prior knowledge.}
	\label{fig:fig1}
\end{center}
\end{figure}

\vspace{-1mm}
This is a challenging task, since the variables affected by the pattern, as well as the number, lengths, and positions of pattern instances, are all unknowns. Further, it is not even clear what objective should be maximized to find this pattern. For example, finding the nearest subsequences under the Euclidean distance yields the incorrect pattern returned by \cite{plmd} (Fig~\ref{fig:fig1}b).
% the nearest subsequences under the Euclidean distance, for example, yield the incorrect pattern returned by \cite{plmd} (Fig~\ref{fig:fig1}b).
% algorithms of \cite{plmd} and \cite{moen} (Fig~\ref{fig:fig1}b).

To overcome these barriers, our technique leverages three observations:
\vspace{-.25mm}
\begin{enumerate}
\itemsep0em
\item Each subsequence of a time series can be seen as having representative features; for example, it may resemble different shapelets \cite{shapelets} or have a particular level of variance.
\item Repeating patterns will cause a disproportionate number of these features to occur together where the pattern happens. We term this set of features affected by the pattern a \textit{flock}. In Figure~\ref{fig:fig1}, the flock would be a characteristic arrangement of spikes in the values of certain variables.
\item If we can identify this flock, we can locate each instance of the pattern with high probability. This holds even in the presence of irrelevant variables (which contribute features excluded from the flock) and unknown instance lengths (which can be inferred based on the interval over which flock features occur together).
\end{enumerate}

\hspace{-10pt}Our contributions consist of:
\vspace{-.25mm}
\begin{itemize}
% \itemsep0em % less spacing; http://tex.stackexchange.com/a/6086
\itemsep-.1mm
\item A formulation of time series motif (i.e., pattern) discovery under more relaxed assumptions than those of existing techniques. In particular, we allow multivariate time series, patterns that affect only subsets of variables, instances of varying lengths, and categorical variables.
\item An $O(N\log(N))$ algorithm to discover patterns under this formulation. It requires less than 300 lines of code, but is considerably faster than the most similar existing algorithms \cite{plmd, moen} and is fast enough to run on batches of data in real time. It is often much more accurate than existing algorithms as well, since many real-world datasets fail to satisfy these algorithms' assumptions. For example, we recognize instances of the above dishwasher pattern with an F1 score of over $90$\%, while the best-performing comparison \cite{plmd} achieves under $30$\%.
\item Open source code and labeled time series that can be used to reproduce and extend our work.
\end{itemize}

% Further, our technique is much more accurate than existing works on real-world data. For example, we detect spoken words with an F1 score of over 90\%, while the next best algorithm achieves less than 30\%.

The remainder of the paper is structured as follows. In Section \ref{sec:problem}, we formally describe our problem and why it is challenging. In Section \ref{sec:relatedWork}, we explain how our problem and approach differ from related work. In Sections \ref{sec:intuition} and \ref{sec:method}, we give the intuition for how and why our technique works, and then describe it in detail. In Sections \ref{sec:results} and \ref{sec:conclusion}, we evaluate it on a battery of real and synthetic datasets and discuss the implications of our results.
