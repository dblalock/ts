
We have carried out what we believe is the most rigorous set of time series motif discovery experiments ever conducted. Further, all of our code is publicly available [], and we have endeavored to make our data cleaning code particularly simple and modular in the hopes that it will be of use to future researchers.
%Below, we discuss the metrics we used to evaluate our algorithm, the baselines against which we compared, the datasets used, and each of our experiments.

All algorithms were implemented in Python using the Scipy stack []. We JIT-compiled the inner loops of the comparison algorithms using Numba [] in order to give them an advantage in speed.

% ------------------------------------------------
\subsection{Evaluation Measures}
% ------------------------------------------------

Let $\mathcal{R}^{\ast}$ be the true set of instance regions and let $\mathcal{R}$ be the set of regions returned by the algorithm being evaluated. Further let $r_1 = (a_1, b_1)$ and $r_2 = (a_2, b_2)$ be two regions.
\begin{Definition}{$Match(r_1, r_2)$. $r_1$ and $r_2$ are said to \textbf{Match} at a threshold of $\tau$ iff $IOU(r_1, r_2) \ge \tau$.
}
\end{Definition}
\begin{Definition}{MatchCount$(\mathcal{R}, \mathcal{R}^{\ast}, \tau)$. The \textbf{MatchCount} of $\mathcal{R}$ given $\mathcal{R}^{\ast}$ and $\tau$ is the greatest number of matches that can be produced by pairing regions in $\mathcal{R}$ with regions in $\mathcal{R}^{\ast}$ such that no region in either set is present in more than one pair. I.e., it is the number of matches when the true and estimated sequences are optimally mapped to one another.\footnote{Since regions (and their possible matches) are ordered in time, this can be computed greedily after sorting $\mathcal{R}$ and $\mathcal{R}^{\ast}$.}
}
\end{Definition}
\begin{Definition}{\textbf{Precision, Recall}, and \textbf{F1 Score}.
}
\end{Definition}
\begin{align}
	Precision_{\tau}(\mathcal{R},& \mathcal{R}^{\ast})
		= MatchCount(\mathcal{R}, \mathcal{R}^{\ast}, \tau) / |\mathcal{R}| \\
	Recall_{\tau}(\mathcal{R}, \mathcal{R}^{\ast})
		&= MatchCount(\mathcal{R}, \mathcal{R}^{\ast}, \tau) / |\mathcal{R}^{\ast}| \\
	F1_{\tau}(\mathcal{R}, \mathcal{R}^{\ast})
		&= \frac{2 * Precision * Recall}{Precision + Recall}
\end{align}
\begin{Definition}{$IOU(r_1, r_2)$. The \textbf{Intersection-Over-Union (IOU)} of $r_1$ and $r_2$ is given by the number of indices $i$ within $r_1$ and $r_2$ divided by the number of indices in either $r_1$ or $r_2$. Formally, let:
}
\end{Definition}
\begin{align}
	Int(r_1, r_2) &= |\{i | a_1 \le i \le b_1 \land a_2 \le i \le b_2\}| \\
	Union(r_1, r_2) &= |\{i | a_1 \le i \le b_1 \lor a_2 \le i \le b_2\}| \\
	IOU(r_1, r_2) &= Int(r_1, r_2) / Union(r_1, r_2)
\end{align}
Further, the IOU of two sets of regions $\mathcal{R}$ and $\mathcal{R}^{\ast}$ is the sums of the intersections over the sums of the unions over all pairs $r_1$ and $r_2$ under the optimal matching.

To evaluate accuracy, we use $IOU(\cdot,\cdot)$, $F1_{.5}(\cdot,\cdot)$ and $F1_{.25}(\cdot,\cdot)$. To evaluate running time, we use seconds of CPU time. All baseline algorithms use the same code for their inner loops, so this measure is fairly robust to implementation bias.

% ------------------------------------------------
\subsection{Baseline Algorithms}
% ------------------------------------------------

Although we reviewed over 70 papers in our search for a solution to this problem, we found that only a few basic techniques have been proposed. This is partially because many works focus on accelerating existing algorithms [] or modifying them to work with slightly different distance measures [], and many simply assume the efficacy of the core pattern discovery techniques and focus their contributions elsewhere [].

Further, as discussed in the Related Work, many of the basic techniques that have been employed do not have clear generalizations to multivariate time series. Moreover, numerous works' contributions center on refinements to the set of patterns discovered [], which depend on accurately finding instances of each pattern in the first place. Thus, since we focus our investigation on finding these patterns, we find that there are only two approaches that merit formal evaluation:
% shorter version of above: inclusion criteria = tested on > 1 dataset with good results and the definition of a "match" given, no pipelines/refinement steps (cuz could throw those at anything), works on multidimensional stuff, no specializing assumptions, such as a particular problem domain, dataset, or pattern property (in particular, the assumption that all instances have identical means and amplitudes and so z-normalization is unnecessary), no quadratic or worse runtimes in N (unless much faster in practice)
% also just emphasize that closest pair tends to dominate the literature and these are the 2 ways to generalize it for a single pattern
\begin{enumerate}
	\item Closest-pair motif discovery, followed by instance selection using a distance threshold []. We find the closest pair efficiently using the lower bound of Mueen [], and determine this threshold using Minnen's algorithm []. We use the latter since it appears to be the only systematic investigation into how this threshold ought to be set and we found that it outperformed other alternatives. We term this algorithm \textit{Thresh}.
	\item Closest-pair motif discovery, followed by instance selection using the Minimum Description Length (MDL) criterion []. We term this algorithm \textit{MDL}.
\end{enumerate}

In both cases, we consider versions of the algorithms that carry out searches at lengths from $M_{min}$ to $M_{max}$, and use the best results from any length. This means lowest distance in the former case and lowest description length in the latter. In other words, we give them the prior knowledge that there is exactly one pattern to be found, rather than rely on the heuristics in the original papers.
%Since it greatly reduces runtime with little or no accuracy impact, we check every fifth length, rather than all of them (despite the fact that this reduces the performance advantage of our own algorithm).

When computing distances between subsequences in these algorithms, we z-normalize within each dimension separately and sum the squared Euclidean distances across dimensions to get the total distance. We then divide by the lengths of the subsequences being considered when comparing distances across lengths.

For both algorithms, we allowed 25\% overlap of instances, since a no-overlap constraint greatly reduced accuracy where instances were densely packed together.

We had to make a modification to the MDL algorithm for it to be effective, so we do not refer to it by its original name (Proper Length Motif Discovery) to avoid confusion. Namely, we did not early abandon the search over lengths when the closest pair at a given length had a negative bitsave---since this was \textit{usually} the case on our real-world data---but instead early abandoned when the best bitsave using any number of instances at a length was negative. Both methods are heuristics for when to stop searching longer lengths; our modification simply prevents it from stopping far too early.

% ------------------------------------------------
\subsection{Datasets}
% ------------------------------------------------

We made use of the following datasets (Fig \ref{fig:datasets}), selected on the basis that they both were publicly available and contained repeated instances of some pattern.

As the reader may observe, some of these datasets could be mined with simpler techniques than those considered here---e.g., an appropriately-tuned peak detector could find many of the instances in the TIDIGITS time series. However, recall that the purpose of our work (and that of the comparisons) is to find patterns \textit{without} the knowledge that they resemble peaks, tend to be close together, etc. Thus, we allow these simplifying characteristics to be present, but refrain from exploiting them.

\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{../figs/paper/datasets}
	\caption{Example time series from each of the datasets used.}
	\label{fig:datasets}
\end{center}
\end{figure}

\subsubsection{MSRC-12}
The MSRC dataset \cite{msrc12} consists of (x,y,z) human joint positions captured by a Microsoft Kinect while subjects repeatedly performed particular motions. Each of the 594 time series in the dataset is 80 dimensional, with a length of 500-1000 after downsampling by a factor of two, for a total of over 25 million sensor readings. There are roughly ten pattern instances per time series, and there tends to be little or no gap between these instances.

Each instance is annotated with a single time step to mark a point during which it was taking place. These annotations tend to be near the middles of pattern instances, but this is inconsistent. We initially used their placement in our evaluations, requiring reported instances to contain exactly one annotation, but found that this added noise to the assessment--the edge cases wherein reported instances happened to exactly fall between annotations dominated the errors of all algorithms evaluated. Consequently, we simply use the number of annotations in a given time series to determine the number of instances that should be reported, and assume that all instances up to this count are positioned properly. Note that this is a boon to the baseline algorithms, which often fail to identify instance starts and ends properly.

\subsubsection{TIDIGITS}
The TIDIGITS dataset \cite{tidigits} is a large collection of human utterances of decimal digits. We use a subset of the data consisting of all recordings containing only one type of digit (e.g., all ``9''s).

Using librosa [], we extracted the first 13 Mel-Frequency Cepstral Coefficients (MFCCs) with a sliding window of length 10ms, stride of 5ms, and FFT length of 512. We then downsampled this densely-sampled data by a factor of 2.

Finally, we shuffled these recordings and concatenated them into longer time series containing at least five digit instances each\footnote{It was not exactly five since we greedily added on utterances and some contained more than one instance--e.g., the subject said ``9'' twice.}.

The resulting time series consist of slightly more silence than speech, with pauses ranging from a second or more to a few milliseconds.

In total we have \%d time series, containing \%d total coefficients and \%d instances.

Because precise annotations regarding when the speaking of each digit instance began and ended are not available, we considered a reported instance to be correct if it fit within the time corresponding to one recording within the concatenated time series.

\subsubsection{Dishwasher}
The Dishwasher dataset [] chronicles energy consumption and related electricity metrics at a power meter connected to a residential dishwasher. It contains twelve variables and two years worth of data sampled once per minute, for a total of 12.6 million data points.

%Crucially, the patterns in this dataset are easily recognizable to a human annotator, including their precise starts and ends. Leveraging this,
We manually plotted, annotated, and verified pattern instances in all 1 million+ of its subsequences.\footnote{see our supporting website [] for a more thorough explanation of how this was done and what the annotations mean.}

Unfortunately, it was not possible to run the baseline algorithms on this raw data since it was too long (indeed, even the optimized implementation of [] present in the original paper took most of a day to mine a length 10,000 time series, and this is 100x larger). Thus, we followed much the same procedure as in other datasets, extracting sets of five pattern instances and concatenating them to form shorter time series. We also extracted a random amount of data around each instance, with length in [25, 200] on either side (the patterns are of length \~150).

\subsubsection{UCR}
Following [], we constructed synthetic datasets by planting examples from the UCR Time Series Archive [] in random walks. We took examples from the 20 smallest datasets, as measured by the length of their objects. For each dataset, we created 50 time series, each containing five objects one class. This yields a total of 1000 time series and 5000 instances.

Note that this is two orders of magnitude larger than the peculiar subset of datasets and handful of examples used in [], which may explain why our results using their algorithm are not as optimistic as their own.

% ------------------------------------------------
\subsection{Instance Discovery Accuracy}
% ------------------------------------------------

The core problem addressed by our work is the robust region of multiple pattern instances within an arbitrary time series. To assess our effectiveness in solving this problem, we evaluated the aforementioned accuracy measures (IOU and F1 score) on each of the four datasets. As shown in Fig [], we significantly outperform the state of the art algorithms in virtually all measures on all datasets. Further, our algorithm returns results an order of magnitude faster.

F1 score at .25 IOU

% \begin{table}[h]
% \caption{F1 score at .25 IOU}
% \centering
\begin{tabular}{llll}
\toprule
{} & Proposed &   MDL & Thresh \\
\midrule
Dishwasher &    0.927 & 0.752 &  0.764 \\
MSRC-12    &    0.944 & 0.925 &  0.719 \\
TIDIGITS   &    0.969 & 0.819 &  0.795 \\
UCR        &    0.588 & 0.603 &  0.626 \\
\bottomrule
\end{tabular}
% \end{table}

% \begin{table}[h]
% \caption{F1 score at .5 IOU}
% \centering

F1 score at .5 IOU

\begin{tabular}{llll}
\toprule
{} & Proposed &   MDL & Thresh \\
\midrule
Dishwasher &    0.788 & 0.059 &  0.092 \\
MSRC-12    &    0.944 & 0.925 &  0.719 \\
TIDIGITS   &    0.969 & 0.819 &  0.795 \\
UCR        &    0.426 & 0.205 &  0.009 \\
\bottomrule
\end{tabular}
% \end{table}

IOU

\begin{tabular}{llll}
\toprule
{} & Proposed &   MDL & Thresh \\
\midrule
Dishwasher &    0.579 & 0.270 &  0.298 \\
MSRC-12    &    0.000 & 0.000 &  0.000 \\
TIDIGITS   &    0.556 & 0.000 &  0.000 \\
UCR        &    0.290 & 0.194 &  0.172 \\
\bottomrule
\end{tabular}

Runtime (s)

\begin{tabular}{llll}
\toprule
{} & Proposed &  MDL & Thresh \\
\midrule
Dishwasher &     1090 & 3912 &   3780 \\
MSRC-12    &    12690 & 7383 &   6959 \\
TIDIGITS   &     5222 & 6061 &  20218 \\
UCR        &     1337 & 6658 &   7207 \\
\bottomrule
\end{tabular}


% ------------------------------------------------
\subsection{Closest Pair Accuracy}
% ------------------------------------------------

We previously claimed that the closest pair of non-overlapping subsequences---the basic quantity sought by many existing algorithms---often does not capture the precise bounds of pattern instances. In this section, we demonstrate this experimentally using the Dishwasher dataset, the UCR dataset, and several synthetic datasets. We constructed the dishwasher and UCR datasets just as described above, but with exactly two instances per time series. We constructed the synthetic datasets by planting pairs of shapes in white noise (Fig []). We then found the closest pair in each time series and compared it to the ground truth.

F1 at .25

\begin{tabular}{lll}
\toprule
{} & Proposed & Thresh \\
\midrule
Dishwasher Pairs &    0.877 &  0.768 \\
UCR Pairs        &    0.616 &  0.585 \\
Synthetic        &    0.972 &  0.767 \\
\bottomrule
\end{tabular}


F1 at .5

\begin{tabular}{lll}
\toprule
{} & Proposed & Thresh \\
\midrule
Dishwasher Pairs &    0.663 &  0.274 \\
UCR Pairs        &    0.359 &  0.459 \\
Synthetic        &    0.705 &  0.433 \\
\bottomrule
\end{tabular}


IOU

\begin{tabular}{lll}
\toprule
{} & Proposed & Thresh \\
\midrule
Dishwasher Pairs &    0.513 &  0.347 \\
UCR Pairs        &    0.265 &  0.340 \\
Synthetic        &    0.621 &  0.459 \\
\bottomrule
\end{tabular}

As shown in Table [], the closest pair almost always detects parts of the instances, but rarely their correct starts and ends. In contrast, our algorithm virtually always discovers the true instance, albeit with a bit of ``extra'' in most cases.

% ------------------------------------------------
\subsection{Scalability}
% ------------------------------------------------

In addition to being more accurate on both real and synthetic data, our algorithm is also much faster. To assess this, we recorded the time it and the comparisons took to run on increasingly long sections of random walk data and the raw Dishwasher data.

\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/scalability/scalability}
	\caption{Stuff is incredibly somewhat scalable}
	\label{fig:scalability}
\end{center}
\end{figure}

In the first row of Fig \ref{fig:scalability}, we vary only the length of the time series ($N$) and keep $M_{min}$ and $M_{max}$ fixed at 100 and 150. In the second row, we hold $N$ constant at 4000 and vary $M_{max}$, with $M_{min}$ fixed at $M_{max}$ - 50 so that the number of lengths searched is constant. In the third row, we fix $N$ at 4000 and set ($M_{min}$, $M_{max}$) to (150,150),(140,160),$\ldots$,(100,200).

All algorithms scale roughly the same with N, since our algorithm uses one invocation of closest pair as a subroutine, but has a much lower constant and scales much more slowly with M and the number of lengths tested.



