

% Before discussing the details of our approach, we first provide an overview and simple example to build the reader's intuition.

% Consider the time series in Figure . It contains two instances of a ``sine wave'' pattern, and we would like the algorithm to return their start and end indices given this data. This example is of course easy, but it is an effective illustration.

% This section focuses on the intuition behind our approach, and the next provides the details.

% In order to return the true set of start and end indices $\mathcal{R}^\ast$, we must deal with four unknowns:
% \begin{enumerate}
% \itemsep-.1em
% \item The number of instances
% \item The start positions of instances
% \item The lengths of instances
% \item The relevant dimensions
% \end{enumerate}
% We describe our algorithm based on how it deals with each of these problems, beginning with a simplistic case and building up to the full technique.

In this section we offer a high-level overview of our technique and the intuition behind it, deferring details to Section~\ref{sec:method}.

The basic steps are given in Algorithm~\ref{algo:flock}. In step 1, we create a representation of the time series that is invariant to instance length and enables rapid pruning of irrelevant dimensions. In step 2, we find sets of regions that approximate the optimal regions $\mathcal{R}^\ast$. In step 3, we select the best set and use them to estimate $\mathcal{R}^\ast$.

Since the difficulty overcome by this technique is the lack of information regarding instance lengths, instance start positions, and relevant dimensions, we elaborate upon these steps by describing how they allow us to deal with each of these unknowns. We begin with a simplified version and build to a sketch of the full algorithm. In particular, we begin by assuming that time series are one-dimensional, instances are nearly identical, and the features used are all characteristic of instances.

\begin{algorithm}[h] % empty algo so that algo numbers stay correct
\caption{Flock}
\label{algo:flock}
\end{algorithm}
\vspace{-3mm}
% \begin{mdframed}[frametitle={\textbf{Algorithm 3:} Flock}]
\begin{mdframed}
\begin{outline}[enumerate]
\1 \textbf{Transform the time series $T$ into a feature matrix $\Phi$}
\vspace{-1mm}
	\2 Select subsequences from the time series to use as shape features
	\2 Transform $T$ into $\Phi$ by encoding the presence of these shapes across time
	\2 Blur $\Phi$ into $\tilde{\Phi}$ to achieve length and time warping invariance

\1 \textbf{Generate sets of windows that may contain pattern instances}
\vspace{-1mm}
	\2 Find ``seed'' windows that are unlikely to have arisen by chance
	\2 Find ``candidate'' windows that resemble each seed
	\2 Rank candidates based on similarity to their seeds

\1 \textbf{Infer the true instances within these candidates}
\vspace{-1mm}
	\2 Greedily construct subsets of candidate windows based on ranks
	\2 Score these subsets and select the best one
	\2 Infer exact instance boundaries within the the selected windows
\end{outline}
\end{mdframed}
% \begin{algorithm}[h]
% \caption{Flock}
% \label{algo:overview}
% \begin{algorithmic}[1]
% % \State {$ \text{Transform each dimension of the time series $T_i$ into}\linebreak \text{ a feature matrix } \Phi_i $}
% % \State {$ \text{Stack the $\Phi_i$ from each dimension into a single $\Phi$} $}
% 	\State Transform the time series $T$ into a feature matrix $\Phi$
% 	\State Generate a set of ``seed'' windows that likely contain \linebreak instances
% 	\State Find sets of windows resembling each seed based on $\Phi$
% 	\State Score each set and return the best
% \end{algorithmic}
% \end{algorithm}

% To elaborate upon these steps, we structure the remainder of this section around dealing with the unknowns plaguing this problem,. Namely:
% \begin{enumerate}
% \itemsep-.1em
% \item The lengths of instances
% \item The relevant dimensions
% \item The number of instances
% \item The start positions of instances
% \end{enumerate}
% We walk through the steps necessary to deal with each in succession, culminating in a sketch of the full algorithm.
% We describe our algorithm based on how it deals with each of these problems, beginning with a simplistic case and building up to the full technique.

% ------------------------------------------------
\subsection{Unknown Instance Lengths}
% ------------------------------------------------
% The problem that has attracted the most attention in the literature \cite{moen, plmd, ratanaFindAllCrap} is that of unknown instance lengths.\footnote{Although these authors make the assumption that that all instances share a single length} To see why not knowing the lengths is problematic, consider the following time series (Fig \ref{fig:lengthsProblem}a).
% Since pattern instances are regions within the overall time series, we find them by searching over windows of data. Because we cannot assume that instances share a common mean or amplitude, it is necessary to normalize the data in these windows []. Unfor
Like virtually all existing work, we find pattern instances by searching over shorter regions of data within the overall time series (Fig \ref{fig:lengthsProblem}a). Since we do not know how long the instances are, this seemingly requires exhaustively searching regions of many lengths \cite{moen, plmd, ratanaFindAllCrap}, so that the instances are sure to be included in the search space.

However, there is an alternative. Our approach is to search over all regions of a \textit{single} length $M_{max}$ (slightly longer than the instances) and then refine these approximate regions. For the moment, we defer details of the refinement process. We refer to this set of regions searched as \textit{windows}, since they correspond to all positions of a sliding window over the time series.
% Our insight is that the regions returned by a search need not \textit{be} instances, but merely \textit{contain} instances. Concretely, our approach is to search regions of a \textit{single} length (slightly longer than the instances) and then infer the instance locations by finding commonalities in the returned set of regions.

This single-length approach presents a challenge: since windows longer than the instances will contain extraneous data, only parts of these windows will appear similar. As an example, consider Fig~\ref{fig:lengthsProblem}. Although the two windows shown contain identical sine waves, the noise around them causes the windows to appear different, as measured by Euclidean distance (the standard measure in most motif discovery work) (Fig~\ref{fig:lengthsProblem}b). Worse, because the data must be mean-normalized for this comparison to be meaningful \cite{mk}, it is not even clear what portions of the regions differ. That is, because the noise has altered the mean, even the would-be identical portions are offset from one another.
% Since we do not know the lengths of instances, however, we are forced to guess an appropriate window length. If it is too short, we will not even consider full instances as candidates. If it is too long, than the similarity of instances within the windows may be drowned out by the dissimilarity of extraneous data around the instances (Fig \ref{fig:lengthsProblem}b), and we may falsely conclude that no instances are present. This seems to necessitate exhaustively trying many window lengths to ensure we test one that works .
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/lengthsProblem}
	\caption{a) A time series containing two pattern instances. b) Including extra data yields large distances (grey lines) between the windows $i$ and $j$ around the pattern instances. c) Comparing based on subsequences within these windows allows close matches between the pieces of the sine waves.}
	\label{fig:lengthsProblem}
\end{center}
\end{figure}

% Suppose that we seek to find the pattern in this time series by computing distances between candidate regions (the basic approach of most all existing algorithms). If we compare regions of too short a length, then we will not even consider the full pattern instances as candidates. If we compare regions of too long a length (Fig \ref{fig:lengthsProblem}b), then even if we compare regions containing \textit{identical} pattern instances, these regions can appear arbitrarily dissimilar thanks to the extra data around these instances.  Since lengths that are either longer or shorter may fail to reveal pattern instances, it seems that we must search exhaustively at many lengths to ensure we test the right one \cite{moen, plmd, ratanaFindAllCrap}.
% However, there is an alternative. Observe that while the subsequences at too long a length appear different when treated as atomic objects, they have many sub-regions (namely, pieces of the sine waves) that are similar when considered in isolation (Fig \ref{fig:lengthsProblem}c). This suggests that if we were to compare time series based on \textit{local} characteristics, instead of their \textit{global} shape, we could search at too long a length and still determine that regions containing pattern instances were similar.
Fortunately, there is a solution. Observe that while the windows appear different when treated as atomic objects, they have many sub-regions (namely, pieces of the sine waves) that are similar when considered in isolation (Fig \ref{fig:lengthsProblem}c). This suggests that if we were to compare the windows based on \textit{local} characteristics, instead of their \textit{global} shape, we could search at too long a length and still determine that windows containing pattern instances were similar.

To enable this, we transform the data into a sparse binary feature matrix that encodes the presence of particular shapes at each position in the time series (Fig \ref{fig:basicTransform}). Columns of the feature matrix are shown at a coarser granularity for visual clarity--in reality, there is one column per sample in the time series. We defer explanation of how these shapes are selected and this feature matrix is constructed to the next section.
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/basicTransform}
	\caption{Feature matrix. Each row is the presence of a particular shape, and each column is a particular time (shown at reduced granularity). Similar regions of data contain the same shapes in roughly the same temporal arrangement.}
	\label{fig:basicTransform}
\end{center}
\end{figure}

Using this feature matrix, we can compare windows of data without knowing the lengths of instances. This is because, even if there is extraneous data at the ends of the windows, there will still be many more common features in the middle where the instances are (Fig \ref{fig:windowTooLong}a) than would be expected by chance.
% This assumes that the window is longer than the instances, which can be ensured using the length bound $M_{max}$.
% Consequently, the two windows will appear more similar than chance under measures such as the dot product and intersection size.
% Since the content of the feature matrix does not depend on the window length and ``extra'' columns will tend to be sparse, we can select a window length \textit{longer than the pattern} and compare regions of data of this length (Fig \ref{fig:windowTooLong}a). These length-agnostic comparisons are what allow us to locate instances without prior knowledge of their lengths.

% (see \S \ref{sec:basicAlgo})
Moreover, once we identify the windows containing instances, we can recover the bounds of the instances by examining which columns in the corresponding windows look sufficiently similar---if a start or end column does not contain a consistent set of 1s across these windows, it is likely not part of the pattern, and we prune it off (Fig \ref{fig:windowTooLong}b).
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/windowTooLong}
	\caption{Because the values in the feature matrix are independent of the window length, a window longer than the pattern can be used to search for instances.}
	\label{fig:windowTooLong}
\end{center}
\end{figure}

As the reader may observe, this illustration is optimistic about the regularity of shapes within instances. In reality, a given shape will not necessarily be present in all instances, and a set of shapes may not appear in precisely the same temporal arrangement more than once because of uniform scaling \cite{keoghUSMotifs} and time warping. We defer treatment of the first point to a later section, but the second can be remedied with a preprocessing step.

To handle both uniform scaling and time warping simultaneously, we ``blur'' the feature matrix in time. The effect is that a given shape is counted as being present over an interval, rather than at a single time step. This is shown in Fig \ref{fig:blur}, using the intersection of the features in two windows as a simplified illustration of how similar they are. Since the blurred features are no longer binary, we depict the ``intersection'' as the elementwise minimum of the windows.
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/blur}
	\caption{Blurring the feature matrix. Despite the second sine wave being longer and warped in time, the two windows still appear similar when blurred.}
	\label{fig:blur}
\end{center}
\end{figure}

% ------------------------------------------------
\subsection{Dealing with Irrelevant Features}
% ------------------------------------------------

One more optimistic subtlety remains at this point. Namely, we have so far assumed that the shapes encoded in the matrix are all pieces of the pattern. In reality, we do not know ahead of time which shapes are part of the pattern, and so there will also be many irrelevant features.

Fortunately, the combination of sparsity and our ``intersection'' operation already causes us to ignore these extra features (Fig~\ref{fig:irrelevantFeatures}). To see this, suppose that the probability of an irrelevant feature being present at a particular location in an instance-containing window by chance is $p_0$. Then the probability of it being present here in $k$ windows is $\approx p_0^k$. Since real feature matrices are over $90$\% zeros, $p_0 \ll .5$, so $p_0^k \approx 0$ even for small $k$.
% To see this, consider two cases. First, if the shape consistently appears with pattern instances, then it is, by definition, part of the pattern. Alternatively, if it rarely or never appears with pattern instances (and thus \textit{should} be ignored) then the intersection operation is almost certain to zero it out. This is because the feature only remains in the intersection if \textit{every} instance-containing window has it in the same place, which is, by definition, unlikely. More precisely, if $p_0$ is the probability of it being present in an instance by chance, then the probability of it being present in $k$ instances is $p_0^k$. Since the matrix is sparse, $p_0$ is typically $< .1$ typically, so
% ---by intersecting the features present in each pattern, such inconsistent features are all but certain to be zeroed out after one or two instances, since there will almost certainly be some instance in which they're absent.
% More precisely, let $p$ be the probability of a feature being asdf TODO fix if $p_0$ is the probability of a feature being nonzero at a given time step by chance, and its presence is only weakly correlated with the pattern happening, then the probability of it being present by chance in $k$ instances is $\approx p_0^k$. Since $p_0$ is small thanks to sparsity (empirically $<.1$), this means that the feature is deemed irrelevant after only a handful of instances. This is of course predicated on our ability to identify which windows contain instances, which we for now just assume.
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/irrelevantFeatures}
	\caption{Most irrelevant features are ignored after only two or three examples, since it is unlikely for them to repeatedly be in the same place in the window by chance. A few ``false positives'' may remain.}
	\label{fig:irrelevantFeatures}
\end{center}
\end{figure}

% In short, by constructing the blurred feature matrix, we can identify pattern instances even in the presence of variations in length and time warping. Our feature matrix will contain many irrelevant features, but by comparing windows using an intersection-like operation, we eliminate these features with high probability after only a handful of instances.

% ------------------------------------------------
\subsection{Multiple Dimensions}
% ------------------------------------------------

% The generalization to multiple dimensions is straightforward: we construct a feature matrix for each dimension and stack them (Fig \ref{fig:newDims}).
The generalization to multiple dimensions is straightforward: we construct a feature matrix for each dimension and concatenate them row-wise. That is, we take the union of the features from each dimension. A dimension may not be relevant, but this just means that it will add irrelevant features. Thanks to the aforementioned combination of sparsity and the intersection operation, we ignore these features with high probability.
% In effect, we the would-be intractable problem of finding the relevant dimensions from $2^D$ possible subsets thereof to the much easier problem of selecting relevant features.
% In fact, we optimize this set of features \textit{exactly} for a given set of instance-containing windows (Section \ref{sec:scoring}).
% \begin{figure}[h]
% \begin{center}
% 	% \includegraphics[width=\linewidth]{paper/newDims}
% 	\includegraphics[width=\linewidth]{paper/newDimsWithIntersection}
% 	\caption{New dimensions just add rows to the feature matrix. If these dimensions are not influenced by the pattern, they add irrelevant features that will be ignored with high probability.}
% 	\label{fig:newDims}
% \end{center}
% \end{figure}

% ------------------------------------------------
\subsection{Finding Instances} \label{sec:basicAlgo}
% ------------------------------------------------

In this section, we provide a sketch of our algorithm for determining which windows contain pattern instances. The idea is that if we are given one window that we know contains an instance, we can generate a set of similar ``candidate'' windows and then determine which of these are likely pattern instances. A summary is given in Algorithm~\ref{algo:findInstancesShort}.
\begin{algorithm}[h]
\caption{FindInstances}
\label{algo:findInstancesShort}
\begin{algorithmic}[1]
\State {$ \text{Oracle provides a window $s$ containing an instance} $}
\State {$ \text{Using $s$, generate ranked list of candidate instances, $\mathcal{C}$} $}
\State {$ \text{Score each prefix $c_1,\ldots,c_i$ of $\mathcal{C}$ and return the best} $}
\end{algorithmic}
\end{algorithm}

In line 1, we assume (for now) that an oracle gives us one ``seed'' window that contains an instance of the pattern. To generate candidates in line 2, we find all other windows with large dot products with this window (Fig \ref{fig:candidates}). To avoid overlap among candidates, we require them to be separated by one half of the window length. These candidates should (and, empirically, do) fully contain all the instances. We order these candidates by decreasing dot product with the seed.

\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/candidates}
	\caption{Given one ``seed'' window containing an instance of the pattern, we find candidate windows that are spaced in time and have a large dot product with the seed window.}
	\label{fig:candidates}
\end{center}
\end{figure}

In line 3, we estimate which subset of these candidates contains the instances. To reduce the search space, we consider only sets that are prefixes of this ranked list.

The score function used to evaluate a set of windows is based on two components. The first is the size of their intersection times the number of windows. This can be understood as the number of nonzeros in the matrix explained by the presence of instances.

The second component is the windows' similarity to data that is deemed not to contain an instance. Specifically, we subtract from the intersection any features that are present in the next best candidate we haven't included (Fig \ref{fig:basicAlgo}). This rewards having a gap in similarity between windows in the set and outside the set. The full score can thus be understood as the number of nonzeros explained by pattern instances minus the number of these nonzeros that could be explained as instances of the next best candidate.
% The idea is that if the current set contains all the pattern instances, then the next candidate should share few features with the current set, and the intersection size will barely decrease; however, if the next window is also a pattern instance, the enemy will have much in common, and so the size of the intersection will greatly decrease. The full score can thus be understood as the number of nonzeros explained by pattern instances minus the number of these nonzeros that could be explained as instances of the nearest enemy.
% \begin{figure}[h]
% \begin{center}
% 	\includegraphics[width=\linewidth]{paper/basicAlgo2}
% 	\caption{Greedy optimization. The score for a set of windows is based on the size of their intersection minus the portion that intersects with the best window that is excluded. Top) scoring with the first candidate in the set. Bottom) scoring with the first and second candidates in the set.}
% 	\label{fig:basicAlgo}
% \end{center}
% \end{figure}
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/basicAlgo_short}
	\caption{Greedy optimization. The score for a set of windows is based on the size of their intersection minus the portion that intersects with the best window that is excluded.}
	\label{fig:basicAlgo}
\end{center}
\end{figure}

Given one window containing an instance, we can use the above procedure to locate the others. So how is this one instance obtained? Again, the key is to generate a set of candidate windows that are a superset of what is needed and take the one that yields the best score. How these candidate seeds are generated is addressed in Section \ref{sec:method}.

% Our complete algorithm, including construction of the feature matrix, runs in $O(DM_{max}N\log(N))$ given a length $N$ time series of $D$ dimensions (see Section \ref{sec:complexity}).

%While our general-purpose shape features provide an off-the-shelf solution for those seeking to use our technique, its versatility means that one could easily specialize it for their domain.

% If one did want to specialize it for a particular domain, rather than use our general-purpose features, doing so would be easy.

% Further, one is not restricted to features of real-valued signals; categorical features like ``day of week'' or ``user gender'' can be employed just as easily.

% We consider this adaptability a major strength of our approach. While we provide and test a simple, general-purpose feature representation that those using our work can employ immediately, the fact that our technique allows incorporation of arbitrary binary features makes it easy to specialize for a particular domain. Unlike techniques based on distances between raw time series, one can easily adapt the features to the task at hand or find patterns spanning numeric and categorical variables.



