To formalize our task, we introduce the following definitions.
% \vspace{-3mm}
\vspace{-2mm}
\begin{Definition}{
\textbf{Time Series}. A $D$-dimensional time series $T$ of length $N$ is a sequence of real-valued vectors $t_1,\ldots,t_N, t_i \in \mathbb{R}^D$. If $D = 1$, we call $T$ ``univariate'' or ``one-dimensional'', and if $D > 1$, we call it ``multivariate'' or ``multi-dimensional''.
}
% \vspace{-3mm}
\vspace{-2mm}
\end{Definition}
\begin{Definition}{\textbf{Region}. A region $R$ is a pair of indices $(a, b), a \le b$. The value $b - a + 1$ is termed the \textbf{length} of the region, and the time series $t_a,\ldots,t_b$ is the \textbf{subsequence} for that region. If a region reflects an occurrence of the pattern, we term the region a pattern \textbf{instance}.
}
\end{Definition}

% ------------------------------------------------
\subsection{Problem Statement}
% ------------------------------------------------
We seek the set of regions that are most likely to have come from a shared ``pattern'' distribution rather than a ``noise'' distribution. This likelihood is assessed based on the subset of features maximizing how distinct these distributions are (using some fixed feature representation).

% Formally, let $x_1,\ldots,x_N$ be the feature representations of all possible regions in a given time series, $\theta_1$ be the ``pattern'' model, $\theta_0$ be the ``non-pattern'' model, and $x_i^\mathcal{F}$ denote the data in region $x_i$ corresponding to the features in set $\mathcal{F}$. We define the optimal set of regions $\mathcal{R}^{\ast}$ as:
% \begin{align} \label{eq:abstractObjective}
% 	\mathcal{R}^{\ast}= \argmax_{\mathcal{R}} \max_{\mathcal{F}, \theta_1} p(\mathcal{R})\prod_{i \in \mathcal{R}} \frac{p(x_i^\mathcal{F} | \theta_1)}{p(x_i^\mathcal{F} | \theta_0)}
% \end{align} % \frac{p(\theta_1)}{p(\theta_0)}
%
% This objective is the odds of each region in $\mathcal{R}$ being an instance of the pattern, with priors on $\theta_1$ and $\theta_0$ included in $p(\mathcal{R})$. It could be maximized for any number of feature representations and models $p(x_i | \theta)$, but we focus on simple binary features and $Bernoulli(p)$ models.
% %We defer the details to Section \ref{sec:method}.
% Concretely, we seek\footnote{See \cite{flockWebsite} for a derivation and more detailed analysis}:

Formally, let $x_1,\ldots,x_K$ be binary feature representations of all $K$ possible regions in a given time series and $x_i^j$ denote feature $j$ in the region $i$. We seek the optimal set of regions $\mathcal{R}^{\ast}$, defined as:
\begin{align} \label{eq:concreteObjective}
	\mathcal{R}^{\ast}= \argmax_{\mathcal{R}} \max_{\mathcal{F}} p(\mathcal{R}) \sum_{j \in \mathcal{F}} c_j (log(\theta_{1j}) - log(\theta_{0j}))
\end{align}
where $\theta_{0j}$ and $\theta_{1j}$ are the empirical probabilities for each feature $j$ in the whole time series and the regions $\mathcal{R}$ respectively, and $c_j$ is the count of feature $j$, ${\sum_{i \in \mathcal{R}} x_i^j}$. $\mathcal{F}$ is the set of features used, which we term the \textit{flock}. The prior $p(R)$ is 0 if regions overlap or violate certain length bounds (see below) and is otherwise uniform.

Equation~\ref{eq:concreteObjective} says that we would like to find regions $i$ and features $j$ such that $x_i^j$ happens both many times (so that $c_j$ is large) and much more often than would occur by chance (so that $log(\theta_{1j}) - log(\theta_{0j})$ is large). In other words, the best flock $\mathcal{F}$ is a large set of features that consistently occur together across many regions, and $\mathcal{R}^{\ast}$ is these regions.

Given certain independencies, this objective is a MAP estimate of the regions and features. Due to space constraints, we defer the details to \cite{flockWebsite}.

% ------------------------------------------------
\subsection{Assumptions}
% ------------------------------------------------
\hspace{-10pt}In contrast to existing work, we DO NOT assume: % no indent
\vspace{-1mm}
\begin{itemize}
\itemsep-.3mm
\item A known number of instances, or that only one pair of instances is needed. %\cite{neverEnding}
% , except that this number is small; i.e., we do not address the (easier) case of dozens or hundreds of instances.
\item A known or constant length for instances.
\item A known or regular spacing between instances.
\item A known set of characteristics shared by instances. In particular, we do not assume that all instances have the same mean and variance, so we cannot bypass normalization when making similarity comparisons.
\item That there is only one dimension.
% \item That dimensions are real-valued.
\item That all dimensions are affected by the pattern.
\item Anything about dimensions not affected by the pattern.
\end{itemize}

\hspace{-10pt}So that the problem is well-defined, we DO assume that:
% Since one of our contributions is relaxing many assumptions made by existing work, we enumerate them in some detail.
\begin{itemize}
\itemsep0em
\item There is exactly one pattern in the data. While real data often contains multiple distinct patterns, we confine the present work to the one pattern case. Note that, with a technique to find single patterns, one can construct an algorithm to find multiple patterns \cite{plmd, moen}.
\item There are at least two instances of the pattern, and no instances overlap in time.
\item There exist bounds $M_{min}$ and $M_{max}$, $M_{min} \ge M_{max}/2$ on the lengths of instances. These bounds disambiguate the case where pairs of adjacent instances could be viewed as single instances of a longer pattern.
\end{itemize}
\vspace{-1mm}
We also do not consider datasets in which instances are rare \cite{rareMotif}---all time series used have instances that collectively comprise at least 10\% of the data.

% ------------------------------------------------
\subsection{Why the task is difficult}
% ------------------------------------------------

The lack of assumptions means that the number of possible sets of regions and relevant dimensions is intractably large. Suppose that we have a $D$-dimensional time series $T$ of length $N$ and $M_{min} \le M \le M_{max}$. There are up to $O(N/M)$ instances, which can collectively start at (at most) $\binom{N}{N/M}$ positions. Further, each can be of $O(M)$ different lengths. Finally, the pattern may affect any of $2^D - 1$ possible subsets of dimensions. Altogether, this means that there are roughly $O(N^{N/M} \cdot M^{N/M} \cdot 2^D)$ combinations of regions and dimensions.
