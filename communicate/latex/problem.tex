To formalize our task, we introduce the following definitions.

\begin{Definition}{
\textbf{Time Series}. A $D$-dimensional time series $T$ of length $N$ is a sequence of real-valued vectors $t_1,\ldots,t_N, t_i \in \mathbb{R}^D$. If $D = 1$, we call $T$ ``univariate'' or ``one-dimensional'', and if $D > 1$, we call it ``multivariate'' or ``multi-dimensional''.
}
\end{Definition}

% \begin{Definition}{\textbf{Position}. A position $i, 1 \le N$ in a time series $T$ of length $N$ is an index into $T$.
% }
% \end{Definition}

\begin{Definition}{\textbf{Region}. A region $R$ is a pair of positions $(i, j), i < j$. The value $j - i + 1$ is termed the \textbf{length} of the region, and the time series $t_i,\ldots,t_j$ is the \textbf{subsequence} for that region. If a region reflects an occurrence of the pattern, we term it a pattern \textbf{instance}.
}
\end{Definition}

% \begin{Definition}{\textbf{Subsequence}. A subsequence $S[i : j]$ of a time series $T$ is the time series $t_i,\ldots,t_j$. It corresponds to the data at a particular region $(i, j)$.
% }
% \end{Definition}

% \begin{Definition}{\textbf{Instance}. An instance is a region in some privileged set $\mathcal{L}$ reflecting where the pattern has happened.
% }
% \end{Definition}

% \begin{Definition}{\textbf{Pattern}. A pattern is some set of characteristics in a time series corresponding to the occurence of some latent phenomenon of interest. A pattern is said to “happen” at each instance.
% }
% \end{Definition}

% ------------------------------------------------
\subsection{Assumptions}
% ------------------------------------------------

Using these definitions, our assumptions can be stated as follows. Since one of our contributions is relaxing many made by existing work, we enumerate them in some detail.

\begin{itemize}
\item There is exactly one pattern in the data. While real data often reflects multiple distinct patterns, we find that existing algorithms struggle on even this simplified problem under our formulation, and so confine the present work to the one pattern case.
\item There exists a latent set of regions $\mathcal{R}^{\ast}$ in which the pattern has truly taken place.
\item These regions have some set of (unknown) distinguishing characteristics.
% I.e., we cannot hope to discover the pattern if it has no effect on the data or perturbs it completely unpredictably.
\item The pattern happens at least twice.
\item The pattern instances do not overlap in time.
\item There exist bounds $M_{min}$ and $M_{max}$, $M_{min} \ge M_{max}/2$ on the lengths of instances. Some other works do not assume such bounds, but our experience is that they are easily determined via cursory inspection of the data or knowledge of the application, and that assuming no such knowledge is unnecessarily complicating the problem.
% Even if these bounds could not be determined and one wanted to brute force search all possible pattern lengths, the fact that one bound is a multiple of the other means that this would require only $O(log(N))$ runs for a length $N$ time series.
\end{itemize}

We do not assume knowledge of:
\begin{itemize}
\item What characteristics distinguish the pattern or hold across its instances. In particular, we do not assume that all instances have the same mean and variance, so we cannot bypass the standard step of normalization when making similarity comparisons.
\item The precise number of times the pattern happens.
% , except that this number is small; i.e., we do not address the (easier) case of dozens or hundreds of instances.
\item How far apart instances of the pattern are.
\item The lengths of instances, except that they are within the aforementioned bounds. In particular, we do not assume that there is a fixed length shared by all instances.
\item Which dimensions are affected by the pattern.
\item Anything about the dimensions that the pattern does not affect.
\end{itemize}

% ------------------------------------------------
\subsection{Problem Statement}
% ------------------------------------------------
Given a time series $T$ and the lower and upper length bounds $M_{min}$ and $M_{max}$, return the set of regions $\mathcal{R}^{\ast}$.

% ------------------------------------------------
\subsection{Why this is difficult}
% ------------------------------------------------

The aforementioned lack of assumptions means that the number of possible sets of regions and relevant dimensions is intractably large.

Suppose that we have a $D$-dimensional time series $T$ of length $N$ and $M_{min} \le M \le M_{max}$. There are up to $O(N/M)$ instances, since this is what fits in $T$ without overlap. These instances can collectively start at (very roughly) $\binom{N}{N/M}$ positions. Further, each can be of $O(M)$ different lengths. Finally, the pattern may affect any of $2^D - 1$ possible subsets of dimensions. Altogether, this means that there are roughly $O(N^{N/M} * M^{N/M} * 2^D)$ combinations of regions and dimensions one might check.

Worse, assessing the wrong regions or dimensions may give us little or no information with which to move forward. If we look for the pattern in the wrong dimensions or in a set of regions in which it does not occur, we could easily miss it or mistakenly hone in on a false pattern.

Another obstacle is the need for normalization when comparing different regions. Because time series are often subject to baseline drift and amplitude variations, there is consensus that it is essential to normalize them based on their mean and variance before making similarity comparisons []. Unfortunately, this normalization means that even two subsequences containing mostly identical data can look extremely different [], since the remaining data could arbitrarily alter their overall means and variances. A consequence of this is that, if the pattern is of length $M$, and we do not search at a length close to $M$, we may miss it \textit{even when examining subsequences where it happened}. This implies a need to search for patterns at many lengths [moen, etc].

%Concretely, if we were to compute that $T[10 : 20]$ is similar to $T[30 : 40]$, it is possible that $T[10 : 25]$ could look nothing like $T[30 : 45]$, since the additional five data points in each could arbitrarily\footnote{There does exist a useful bound on the change in Euclidean distances when more data is added given finite variance \cite{moen}, but this does not solve the basic issue that these subsequences may look wildly different} change the mean and variance of the overall subsequence (and thus the values to which the first ten points were normalized). The same goes for removing data---$T[10 : 15]$ could also bear no resemblence to $T[30 : 35]$.


% Another consequence is that we cannot simply discretize the time series and exploit string mining or frequent pattern mining techniques, since discretizing at the wrong length could obscure the pattern.

In short, this task is plagued by three chicken-and-egg problems. To accurately infer the pattern, we must use 1) the right regions, 2) the right dimensions, and 3) the right length. However, to know what any of these are, we (seemingly) must know the pattern.
% \begin{enumerate}
% \item We must infer the pattern using the right regions, but we can only select these regions if we know the pattern.
% \item We must search the right dimensions, but we only know these dimensions once we know the pattern.
% \item We must search at the right length, but we only know this length once we know the pattern.
% \end{enumerate}

Fortunately, we describe a technique that bypasses all three of these problems, identifying the pattern, its regions, and the relevant dimensions in a constant number of passes over the data.

