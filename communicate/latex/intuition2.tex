

% Before discussing the details of our approach, we first provide an overview and simple example to build the reader's intuition.

% Consider the time series in Figure . It contains two instances of a ``sine wave'' pattern, and we would like the algorithm to return their start and end indices given this data. This example is of course easy, but it is an effective illustration.

% This section focuses on the intuition behind our approach, and the next provides the details.

% In order to return the true set of start and end indices $\mathcal{R}^\ast$, we must deal with four unknowns:
% \begin{enumerate}
% \itemsep-.1em
% \item The number of instances
% \item The start positions of instances
% \item The lengths of instances
% \item The relevant dimensions
% \end{enumerate}
% We describe our algorithm based on how it deals with each of these problems, beginning with a simplistic case and building up to the full technique.

In this section we offer a high-level overview of our technique and the intuition behind it, deferring details to Section~\ref{sec:method}.

The basic steps are given in Algorithm~\ref{algo:flock}. In step 1, we create a representation of the time series that is invariant to instance length and enables rapid pruning of irrelevant dimensions. In step 2, we find sets of regions that may contain pattern instances. In step 3, we refine these sets to estimate the optimal instances $\mathcal{R}^\ast$.

Since the main challenge overcome by this technique is the lack of information regarding instance lengths, instance start positions, and relevant dimensions, we elaborate upon these steps by describing how they allow us to deal with each of these unknowns. We begin with a simplified approach and build to a sketch of the full algorithm. In particular, we begin by assuming that time series are one-dimensional, instances are nearly identical, and all features are useful.

\begin{algorithm}[h] % empty algo so that algo numbers stay correct
\caption{Flock}
\label{algo:flock}
\end{algorithm}
\vspace{-3mm}
% \begin{mdframed}[frametitle={\textbf{Algorithm 3:} Flock}]
\begin{mdframed}
\begin{outline}[enumerate]
\1 \textbf{Transform the time series $T$ into a feature matrix $\Phi$}
\vspace{-1mm}
	\2 Randomly select subsequences from the time series to use as shape features
	\2 Transform $T$ into $\Phi$ by encoding the presence of these shapes across time
	\2 Blur $\Phi$ to achieve length and time warping invariance

\1 \textbf{Using $\Phi$, generate sets of ``candidate'' windows that may contain pattern instances}
\vspace{-1mm}
	\2 Find ``seed'' windows that are unlikely to have arisen by chance
	\2 Find ``candidate'' windows that resemble each seed
	\2 Rank candidates based on similarity to their seeds

\1 \textbf{Infer the true instances within these candidates}
\vspace{-1mm}
	\2 Greedily construct subsets of candidate windows based on ranks
	\2 Score these subsets and select the best one
	\2 Infer exact instance boundaries within the selected windows
\end{outline}
\end{mdframed}
% \begin{algorithm}[h]
% \caption{Flock}
% \label{algo:overview}
% \begin{algorithmic}[1]
% % \State {$ \text{Transform each dimension of the time series $T_i$ into}\linebreak \text{ a feature matrix } \Phi_i $}
% % \State {$ \text{Stack the $\Phi_i$ from each dimension into a single $\Phi$} $}
% 	\State Transform the time series $T$ into a feature matrix $\Phi$
% 	\State Generate a set of ``seed'' windows that likely contain \linebreak instances
% 	\State Find sets of windows resembling each seed based on $\Phi$
% 	\State Score each set and return the best
% \end{algorithmic}
% \end{algorithm}

% To elaborate upon these steps, we structure the remainder of this section around dealing with the unknowns plaguing this problem,. Namely:
% \begin{enumerate}
% \itemsep-.1em
% \item The lengths of instances
% \item The relevant dimensions
% \item The number of instances
% \item The start positions of instances
% \end{enumerate}
% We walk through the steps necessary to deal with each in succession, culminating in a sketch of the full algorithm.
% We describe our algorithm based on how it deals with each of these problems, beginning with a simplistic case and building up to the full technique.

% ------------------------------------------------
\vspace{-2mm}
\subsection{Unknown Instance Lengths}
% ------------------------------------------------
% The problem that has attracted the most attention in the literature \cite{moen, plmd, ratanaFindAllCrap} is that of unknown instance lengths.\footnote{Although these authors make the assumption that that all instances share a single length} To see why not knowing the lengths is problematic, consider the following time series (Fig \ref{fig:lengthsProblem}a).
% Since pattern instances are regions within the overall time series, we find them by searching over windows of data. Because we cannot assume that instances share a common mean or amplitude, it is necessary to normalize the data in these windows []. Unfor
Like most existing work, we find pattern instances by searching over shorter regions of data within the overall time series (Fig \ref{fig:lengthsProblem}a). Since we do not know how long the instances are, this seemingly requires exhaustively searching regions of many lengths \cite{moen, plmd, ratanaFindAllCrap}, so that the instances are sure to be included in the search space.

However, there is an alternative. Our approach is to search over all regions of a \textit{single} length $M_{max}$ (the maximum possible instance length) and then refine these approximate regions. For the moment, we defer details of the refinement process. We refer to this set of regions searched as \textit{windows}, since they correspond to all positions of a sliding window over the time series.
% Our insight is that the regions returned by a search need not \textit{be} instances, but merely \textit{contain} instances. Concretely, our approach is to search regions of a \textit{single} length (slightly longer than the instances) and then infer the instance locations by finding commonalities in the returned set of regions.

This single-length approach presents a challenge: since windows longer than the instances will contain extraneous data, only parts of these windows will appear similar. As an example, consider Figure~\ref{fig:lengthsProblem}. Although the two windows shown contain identical sine waves, the noise around them causes the windows to appear different, as measured by Euclidean distance (the standard measure in most motif discovery work) (Fig~\ref{fig:lengthsProblem}b). Worse, because the data must be mean-normalized for this comparison to be meaningful \cite{mk}, it is not even clear what portions of the regions are similar or different---because the noise has altered the mean, even the would-be identical portions are offset from one another.
% Since we do not know the lengths of instances, however, we are forced to guess an appropriate window length. If it is too short, we will not even consider full instances as candidates. If it is too long, than the similarity of instances within the windows may be drowned out by the dissimilarity of extraneous data around the instances (Fig \ref{fig:lengthsProblem}b), and we may falsely conclude that no instances are present. This seems to necessitate exhaustively trying many window lengths to ensure we test one that works .
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/lengthsProblem}
	\caption{a) A time series containing two pattern instances. b) Including extra data yields large distances (grey lines) between the windows $i$ and $j$ around the pattern instances. c) Comparing based on subsequences within these windows allows close matches between the pieces of the sine waves.}
	\label{fig:lengthsProblem}
\end{center}
\end{figure}

% Suppose that we seek to find the pattern in this time series by computing distances between candidate regions (the basic approach of most all existing algorithms). If we compare regions of too short a length, then we will not even consider the full pattern instances as candidates. If we compare regions of too long a length (Fig \ref{fig:lengthsProblem}b), then even if we compare regions containing \textit{identical} pattern instances, these regions can appear arbitrarily dissimilar thanks to the extra data around these instances.  Since lengths that are either longer or shorter may fail to reveal pattern instances, it seems that we must search exhaustively at many lengths to ensure we test the right one \cite{moen, plmd, ratanaFindAllCrap}.
% However, there is an alternative. Observe that while the subsequences at too long a length appear different when treated as atomic objects, they have many sub-regions (namely, pieces of the sine waves) that are similar when considered in isolation (Fig \ref{fig:lengthsProblem}c). This suggests that if we were to compare time series based on \textit{local} characteristics, instead of their \textit{global} shape, we could search at too long a length and still determine that regions containing pattern instances were similar.
However, while the windows appear different when treated as atomic objects, they have many sub-regions (namely, \\ pieces of the sine waves) that are similar when considered in isolation (Fig \ref{fig:lengthsProblem}c). This suggests that if we were to compare the windows based on \textit{local} characteristics, instead of their \textit{global} shape, we could search at a length longer than the pattern and still determine that windows containing pattern instances were similar.

To enable this, we transform the data into a sparse binary feature matrix that encodes the presence of particular shapes at each position in the time series (Fig \ref{fig:basicTransform}). Columns of the feature matrix are shown at a coarser granularity for visual clarity--in reality, there is one column per sample in the time series. We defer explanation of how these shapes are selected and this feature matrix is constructed to the next section.
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/basicTransform}
	\caption{Feature matrix. Each row is the presence of a particular shape, and each column is a particular time (shown at reduced granularity). Similar regions of data contain the same shapes in roughly the same temporal arrangement.}
	\label{fig:basicTransform}
\end{center}
\end{figure}

Using this feature matrix, we can compare windows of data without knowing the lengths of instances. This is because, even if there is extraneous data at the ends of the windows, there will still be more common features in the middle (Fig \ref{fig:windowTooLong}a) than would be expected by chance.
% This assumes that the window is longer than the instances, which can be ensured using the length bound $M_{max}$.
% Consequently, the two windows will appear more similar than chance under measures such as the dot product and intersection size.
% Since the content of the feature matrix does not depend on the window length and ``extra'' columns will tend to be sparse, we can select a window length \textit{longer than the pattern} and compare regions of data of this length (Fig \ref{fig:windowTooLong}a). These length-agnostic comparisons are what allow us to locate instances without prior knowledge of their lengths.

% (see \S \ref{sec:basicAlgo})
Once we identify the windows containing instances, we can recover the bounds of the instances by examining which columns in the corresponding windows look sufficiently \\ similar---if a start or end column does not contain a consistent set of 1s across these windows, it is likely not part of the pattern, and we prune it (Fig \ref{fig:windowTooLong}b).
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/windowTooLong}
	\caption{Because the values in the feature matrix are independent of the window length, a window longer than the pattern can be used to search for instances.}
	\label{fig:windowTooLong}
\end{center}
\end{figure}

At present, however, this illustration is optimistic about the regularity of shapes within instances. In reality, a given shape will not necessarily be present in all instances, and a set of shapes may not appear in precisely the same temporal arrangement more than once because of uniform scaling \cite{keoghUSMotifs} and time warping. We defer treatment of the first point to a later section, but the second can be remedied with a preprocessing step.

To handle both uniform scaling and time warping simultaneously, we ``blur'' the feature matrix in time. The effect is that a given shape is counted as being present over an interval, rather than at a single time step. This is shown in Figure \ref{fig:blur}, using the intersection of the features in two windows as a simplified illustration of how similar they are. Since the blurred features are no longer binary, we depict the ``intersection'' as the elementwise minimum of the windows.
\vspace{-3mm}
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/blur}
	\caption{Blurring the feature matrix. Despite the second sine wave being longer and warped in time, the two windows still appear similar when blurred.}
	\label{fig:blur}
\end{center}
\end{figure}

% ------------------------------------------------
\vspace{-3mm}
\subsection{Dealing with Irrelevant Features}
% ------------------------------------------------

This example has so far assumed that the shapes encoded in the matrix are all pieces of the pattern. In reality, we do not know ahead of time which shapes are part of the pattern, and so there will also be many irrelevant features.

Fortunately, the combination of sparsity and our ``intersection'' operation causes us to ignore these extra features (Fig~\ref{fig:irrelevantFeatures}). To see this, suppose that the probability of an irrelevant feature being present at a particular location in an instance-containing window is $p_0$. Then the probability of it being present by chance in $k$ windows is $\approx p_0^k$. Feature matrices for real data are over $90$\% zeros, since a given subsequence can only resemble a few shapes. Consequently, $p_0$ is small (e.g., $0.05$), and $p_0^k \approx 0$ even for small $k$.

% so $p_0$ is small (e.g., $0.05$). Consequently, $p_0^k \approx 0$ even for small $k$.
% Since feature matrices for real data are over $90$\% zeros, $p_0$ is small (e.g., $0.05$), so $p_0^k \approx 0$ even for small $k$.
% To see this, consider two cases. First, if the shape consistently appears with pattern instances, then it is, by definition, part of the pattern. Alternatively, if it rarely or never appears with pattern instances (and thus \textit{should} be ignored) then the intersection operation is almost certain to zero it out. This is because the feature only remains in the intersection if \textit{every} instance-containing window has it in the same place, which is, by definition, unlikely. More precisely, if $p_0$ is the probability of it being present in an instance by chance, then the probability of it being present in $k$ instances is $p_0^k$. Since the matrix is sparse, $p_0$ is typically $< .1$ typically, so
% ---by intersecting the features present in each pattern, such inconsistent features are all but certain to be zeroed out after one or two instances, since there will almost certainly be some instance in which they're absent.
% More precisely, let $p$ be the probability of a feature being asdf TODO fix if $p_0$ is the probability of a feature being nonzero at a given time step by chance, and its presence is only weakly correlated with the pattern happening, then the probability of it being present by chance in $k$ instances is $\approx p_0^k$. Since $p_0$ is small thanks to sparsity (empirically $<.1$), this means that the feature is deemed irrelevant after only a handful of instances. This is of course predicated on our ability to identify which windows contain instances, which we for now just assume.
\begin{figure}[h]
\begin{center}
	\includegraphics[width=\linewidth]{paper/irrelevantFeatures}
	\caption{Most irrelevant features are ignored after only two or three examples, since it is unlikely for them to repeatedly be in the same place in the window. A few ``false positives'' may remain.}
	\label{fig:irrelevantFeatures}
\end{center}
\end{figure}

% In short, by constructing the blurred feature matrix, we can identify pattern instances even in the presence of variations in length and time warping. Our feature matrix will contain many irrelevant features, but by comparing windows using an intersection-like operation, we eliminate these features with high probability after only a handful of instances.

% ------------------------------------------------
\vspace{-1mm}
\subsection{Multiple Dimensions}
% ------------------------------------------------

% The generalization to multiple dimensions is straightforward: we construct a feature matrix for each dimension and stack them (Fig \ref{fig:newDims}).
The generalization to multiple dimensions is straightforward: we construct a feature matrix for each dimension and concatenate them row-wise. That is, we take the union of the features from each dimension. A dimension may not be relevant, but this just means that it will add irrelevant features. Thanks to the aforementioned combination of sparsity and the intersection operation, we ignore these features with high probability.
% In effect, we the would-be intractable problem of finding the relevant dimensions from $2^D$ possible subsets thereof to the much easier problem of selecting relevant features.
% In fact, we optimize this set of features \textit{exactly} for a given set of instance-containing windows (Section \ref{sec:scoring}).
% \begin{figure}[h]
% \begin{center}
% 	% \includegraphics[width=\linewidth]{paper/newDims}
% 	\includegraphics[width=\linewidth]{paper/newDimsWithIntersection}
% 	\caption{New dimensions just add rows to the feature matrix. If these dimensions are not influenced by the pattern, they add irrelevant features that will be ignored with high probability.}
% 	\label{fig:newDims}
% \end{center}
% \end{figure}

% ------------------------------------------------
% \vspace{-1mm}
\subsection{Finding Instances} \label{sec:basicAlgo}
% ------------------------------------------------
The previous subsections have described how we construct the feature matrix. In this section, we describe how to use this matrix to find pattern instances. A summary is given in Algorithm~\ref{algo:findInstancesShort}. The idea is that if we are given one ``seed'' window that contains an instance, we can generate a set of similar ``candidate'' windows and then determine which of these are likely pattern instances. Since we cannot generate seeds that are certain to contain instances, we generate many seeds and try each. We defer explanation of how seeds are generated to the next section.

\begin{algorithm}[h]
% \caption{$FindInstances(\mathcal{S}, \mathcal{X})$ \\
% 	\hspace{1.5em} $S$ - a set of ``seed'' windows \\
% 	\hspace{10mm} $\mathcal{X}$ - the set of all windows}
% \caption{$FindInstances(\mathcal{S}, \mathcal{\tilde{X}})$ }
\caption{$FindInstances(\mathcal{S}, \mathcal{X})$ }
\label{algo:findInstancesShort}
\begin{algorithmic}[1]

% \State \textbf{Input:} $S$ - a set of ``seed'' windows
% \State \textbf{Input:} $\mathcal{X}$ - the set of all windows
% \State \textbf{Input:} $S$, ``seed'' windows; $\mathcal{\tilde{X}}$, all blurred windows
\State \textbf{Input:} $S$, ``seed'' windows; $\mathcal{X}$, all blurred windows

\State $\mathcal{I}_{best} \leftarrow \{\}$
\State $score_{best} \leftarrow -\infty$
\For {each seed window $s \in \mathcal{S}$}
	\LineComment{Find candidate windows $\mathcal{C}$ based on}
	\LineComment{dot product with seed window $s$}
	% \State $P \leftarrow [s \cdot \tilde{x}_i, \tilde{x}_i \in \mathcal{\tilde{X}}]$
	\State $P \leftarrow [s \cdot \tilde{x}_i, \tilde{x}_i \in \mathcal{X}]$
	% \State $\mathcal{C} \leftarrow spacedLocalMaxima(P, M_{min})$
	\State $\mathcal{C} \leftarrow localMaxima(P)$
	\State $\mathcal{C} \leftarrow enforceMinimumSpacing(C, M_{min})$
	\State $\mathcal{C} \leftarrow sortByDescendingDotProd(\mathcal{C}, P)$
	\LineComment{assess subsets of $\mathcal{C}$}
	\For {$k \leftarrow 2,\ldots,|\mathcal{C}|$}
		\State $\mathcal{I} \leftarrow \{c_1,...,c_k\}$ \COMMENT{$k$ best candidates}
		\State $score \leftarrow computeScore(\mathcal{I}, c_{k+1})$
		\If { $score > score_{best}$ }
			\State $score_{best} \leftarrow score$
			\State $\mathcal{I}_{best} \leftarrow \mathcal{I}$
		\EndIf
	\EndFor
\EndFor
\Return $\mathcal{I}_{best}$
\vspace{-1mm}
\end{algorithmic}
\end{algorithm}

The main loop iterates through all seeds $s$ and generates sets of candidate windows for each. These candidates are the windows whose dot products with $s$ are local maxima---i.e., they are higher than those of the windows just before and after. To prevent excess overlap, a minimum spacing is enforced between the candidates by only taking the best relative maximum in any interval of width $M_{min}$ (the instance length lower bound). If $s$ contains a pattern instance, the resulting candidates should be (and typically are) a superset of the true instance-containing windows.

In the inner loop, we assess subsets of the candidates to determine which ones contain instances. Since there are $2^{|\mathcal{C}|} = O(2^{N/M_{min}})$ possible subsets, we use a greedy approach that tries only $|\mathcal{C}| = O(N/M_{min})$ subsets. Specifically, we rank the candidates based on their dot products with $s$ and assess subsets that contain the $k$ highest-ranking candidates for each possible $k$.

The final set returned is the highest-scoring subset of candidates for any seed. See Section~\ref{sec:scoring} for an explanation of the scoring function.
