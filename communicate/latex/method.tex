
Our basic approach consists of mapping time series to sparse, boolean-valued vectors, and then finding patterns as ``suspicious coincidences'' in these vectors. To explain it in detail, we first describe what constitutes a coincidence in such a set of vectors, then why this task maps well to multivariate pattern discovery, and, finally, how we reduce the time series problem to this case.

% ------------------------------------------------
\subsection{Coincidences}
% ------------------------------------------------

Suppose that we are seeking to discover a ``pattern'' in sparse Boolean-valued data. The nature of this pattern is that, when present in a given object, it will cause certain (unknown) variables to be 1 with high probability. We do not know which variables or objects are affected, although we can assume that it is at least one variable and at least two objects. We do not know what processes generate the data that is not affected by the pattern, so for now, we will assume that each variable is drawn from a Bernoulli distribution, and that this distribution usually produces 0s.

It is not obvious how to solve this problem, but it is reasonably intuitive what the right answer ought to look like. Consider the following dataset:
\begin{center}
{\setstretch{1.2}

$x_1 =$ [0,0,0,0,0]

$x_2 =$ [{\color{blue}\textbf{1,1}},0,0,{\color{blue}\textbf{1}}]

$x_3 =$ [{\color{blue}\textbf{1,1}},0,1,{\color{blue}\textbf{1}}]

$x_4 =$ [0,0,0,1,0]

$x_5 =$ [{\color{blue}\textbf{1,1}},1,0,{\color{blue}\textbf{1}}]

$x_6 =$ [0,1,1,1,0]

}
\end{center}

If none of these vectors had more than one or two 1s in common, we might conclude that the data was random and there was no pattern to be had here. However, we see that that are three different vectors, $x_2, x_3$, and $x_5$, that all share three 1s. This is unlikely to happen by chance in a sample of this size, so we conclude that there is a pattern---1s in the first, second, and fifth features---present in these three vectors.

To generalize then, our task is to find a set of object indices $\mathcal{I}^{\ast}$ and features $\mathcal{F}^{\ast}$ such that the probability of getting these features' values in these objects by chance is minimized. Since the data is sparse, this effectively means finding repeating groups of 1s, since groups of 0s are to be expected.
%Visually, the intuition is that we seek to find the biggest ``block'' of 1s in the data we can (if we are free to rearrange the order of features and examples).

Formally, we are given a collection $\mathcal{X}$ of $D$-dimensional vectors, $x_1,\ldots,x_N, x_i \in \mathbb{R}^D$. Let $x_i^j$ denoting the $j$th feature of $x_i$, and $x_i^j \sim Bernoulli(p_0)$ for some small $p_0$. Our task is to find:

% Suppose we are given a collection $\mathcal{X}$ of $N$ $D$-dimensional vectors, $x_1,\ldots,x_N, x_i \in \mathbb{R}^D$, with $x_i^j$ denoting the $j$th feature of $x_i$. Now suppose that we have a model of what vectors are likely to arise by chance. For now, this model is that each feature is a Bernoulli random variable; i.e. $x_i^j \sim Bernoulli(p_0)$ for some small $p_0$, so that there are expected to be mostly zeros.

% Intuitively, what would it mean to find a ``suspicious coincidence'' in such data? To see, consider the following toy example. We have six vectors, $x_1,\ldots,x_6$, each with five dimensions, and assume $p_0 = 1/5$, so that we'd expect a single 1 in each vector:

% \begin{center}
% {\setstretch{1.2}

% $x_1 =$ [0,0,0,0,0]

% $x_2 =$ [{\color{blue}\textbf{1,1}},0,0,{\color{blue}\textbf{1}}]

% $x_3 =$ [{\color{blue}\textbf{1,1}},0,1,{\color{blue}\textbf{1}}]

% $x_4 =$ [0,0,0,1,0]

% $x_5 =$ [{\color{blue}\textbf{1,1}},1,0,{\color{blue}\textbf{1}}]

% $x_6 =$ [0,1,1,1,0]

% }
% \end{center}

% If no vectors had more than one or two 1s in common, we might conclude that the data was random and there was no suspicious coincidence to be had here. However, we see that that are three different vectors $x_1,\ldots,x_3$, that all share three ones. This is unlikely to happen by chance in a sample of this size, so we conclude that there is a pattern---three initial ones---present in some of the vectors.

% Note that there are four reasons why this is a coincidence:
% \begin{enumerate}
% \item \textbf{Quantity}. The vectors had many 1s in common. Had it only been a single 1, this could well have been chance.
% \item \textbf{Frequency}. There were many vectors. Three 1s in two vectors may have been suspicious, but three ones in three of them was even more so.
% \item \textbf{Consistency}. The 1s were consistently present. Had each 1 only been present in two of the three, it again would have been less compelling.
% \item \textbf{Base Rate}. 1s were not expected to be common. If we had expected mostly 1s, having three in common would have been less surprising.
% \end{enumerate}

% Formally, we seek to find:
\begin{align}
	\mathcal{I}^{\ast}, \mathcal{F}^{\ast} = \argmax_{\mathcal{I}, \mathcal{F}} \prod_{i \in \mathcal{I}} \prod_{j \in \mathcal{F}} \frac{p(x_i^j | \theta_{1j})}{p(x_i^j | \theta_{0j})}
\end{align}
where $\theta_0$ and $\theta_1$ are the parameters of our ``noise'' model and ``pattern'' model, respectively. $\theta_{0j} = p_0$, and $\theta_{1j}$ is the empirical fraction of times that $x_i^j = 1$. The latter is the case because this quantity is the maximum likelihood estimate, which maximizes this ratio.

This expression can be simplified into a more manageable form. First, since each feature is either 0 or 1, we can expand the above to get:
\begin{align}
	\argmax_{\mathcal{I}, \mathcal{F}} \prod_{i \in \mathcal{I}} \prod_{j \in \mathcal{F}} \frac{\theta_{1j}^{I\{x_i^j = 1\}}(1 - \theta_{1j})^{I\{x_i^j = 0\}}}{\theta_{0j}^{I\{x_i^j = 1\}}(1 - \theta_{0j})^{I\{x_i^j = 0\}}}
\end{align}
Letting $c_j$ denote the number of times feature $j$ is 1, ${\sum_{i \in \mathcal{I}} x_i^j}$, and $k$ denote $|\mathcal{I}|$, this can be rewritten as:
\begin{align}
	\argmax_{\mathcal{I}, \mathcal{F}} \prod_{j \in \mathcal{F}} \frac{\theta_{1j}^{c_j}(1 - \theta_{1j})^{(k - c_j)}}{\theta_{0j}^{c_j}(1 - {\theta_0j})^{(k - c_j)}}
\end{align}
It is easier (and equivalent) to maximize the log of this expression:
\begin{align}
	\argmax_{\mathcal{I}, \mathcal{F}} \sum_{j \in \mathcal{F}} [c_j (log(\theta_{1j}) - log(\theta_{0j})) \\
	+ (k-c_j)(log(1-\theta_{1j}) - log(1-\theta_{0j})) ]
\end{align}
Observe that the first difference of logs is positive when $x_j$ is more likely to be 1 than chance, and the second is positive when it is less likely. Since the data is expected to be sparse to begin with, features that occur even less than chance must almost never happen, and so we would be overfitting to include them. E.g., if the 5th feature is 1 only 2\% of the time, and doesn't appear in the three $x_i$ we have included, we should not conclude that its absence is meaningful. Dropping the latter term, then, we get the following simple objective function:
\begin{align}
	\argmax_{\mathcal{I}, \mathcal{F}} \sum_{j \in \mathcal{F}} c_j (log(\theta_{1j}) - log(\theta_{0j}))
\end{align}

This says that we would like to find a set of $x_i$ and features $j$ such that $x_i^j$ happens both many times (so that $c_j$ is large) and much more often than chance (so that $log(\theta_{1j}) - log(\theta_{0j})$ is large).

% A good answer would return a set of examples and features that would be unlikely to occur by chance. For now, assume that chance it simply a Bernoulli random variable for each feature; i.e. xj ~ Bernoulli(p) for some small p, so that there are expected to be mostly zeros. In this example,

% ------------------------------------------------
\subsection{Why sparse booleans?}
% ------------------------------------------------

The above objective function may be simple, but it entails a combinatorial subset selection problem that is not easy to optimize. Why then is this problem formulation desirable?

The first reason is that it allows us to infer the relevant variables from extremely few examples. Suppose that an oracle tells us that $x_1 = [1,1,0,1,1]$ and $x_2 = [1,1,1,0,1]$ are instances of the pattern. Since it is unlikely that \textit{any} of the variables would be one in both vectors (due to sparsity), we can immediately infer with high confidence that most of these features are part of the pattern.

Now suppose that variables three and four are not actually part of the pattern, but one, two and five are. Then with high probability (since non-pattern variables are typically 0), the next instance the oracle hands us will be something like $x_3 = [1,1,0,0,1]$, in which these variables are 0, and we will correctly suspect that these variables are not part of the pattern after all.

This example is of course contrived and somewhat optimistic, but the underlying mathemetics are potent. If a feature $j$ is 1 with probability $\theta_{1j}$ when part of the pattern and $\theta_{0j}$ when not part of the pattern, the odds of it being part of the pattern given that it's 1 in all of $k$ objects is $(\frac{\theta_{1j}}{\theta_{0j}})^k$, which rapidly increases even if $\theta_{1j}$ is only slightly greater than $\theta_{0j}$. More generally, if the feature is 1 in $c$ out of $k$ objects, the probabilities of it being from a pattern and noise are drawn from $Binomial(c, k)$ distributions, which quickly narrow as $c$ and $k$ increase.

The second reason this formulation is desirable, which follows from the same logic, is that it allows us to infer the correct pattern from very few examples. Since we can compute the above probabilities across each feature, we can directly compare candidate patterns based on how likely their variables are to have occurred by chance, and take the best one. Since these probabilities are (roughly) exponential in the number of features and number of instances, it is almost certain that one pattern will be a clear winner.

% The third reason is that boolean variables, unlike real-valued variables, are amenable to While boolean variables are arguably not the most natural representation of real-valued data, their mathematical simplicity and computational efficiency

Finally, this formulation maps naturally to our original problem. Instead of having some subset of vectors $x_i$ that are instances of the pattern, we have some subset of regions. Instead of some subset of variables in the vector that the pattern affects, we have some subset of dimensions in the time series. Most importantly, in both cases, we must infer the pattern from very few examples.

While Boolean variables are arguably not the most natural representation of real-valued data (and the above properties could be achieved with other distributions), Booleans are mathematically simple, computationally efficient, and, above all, require us to learn only a single parameter per variable, $\theta_{1j}$. We find that this last requirement is vital in preventing overfitting, since even standard deviations are hard to compute with the small numbers of examples we consider.

In short, by mapping time series to sparse Boolean vectors, we can robustly bring to bear the power of exponentially increasing probabilities when attempting to infer the pattern.

% ------------------------------------------------
\subsection{Optimizing the objective}
% ------------------------------------------------

Due to its combinatorial nature, the above objective is intractable to optimize exactly. However, we find that a simple greedy algorithm works extremely well in practice. The insight is that pattern instances will necessarily share many 1s, and therefore have large dot products. Using this fact, we can find groups of objects with large dot products as candidates for $\mathcal{I}$ and then infer a good $\mathcal{F}$ for each group. After examining a constant number of groups, we return the best $\mathcal{I}$ and $\mathcal{F}$.

In more detail, assume that we are given a constant size set $\mathcal{S}$ of ``seed'' indices $s$ such that we suspect $x_s$ is an instance of the pattern. We discuss where these seeds come from when discussing the case of time series.
%Most of these seeds will not be pattern instances, but we are confident that at least one is. We discuss where these seeds come from when discussing the case of time series.

% It is desirable not to consider all possible x_i as candidates We can compute the dot product $\epsilon$ that would be expected to occur by chance between two vectors as:
% \begin{align}
% 	\epsilon = D * p_0^2
% \end{align}
% where $p_0$ remains the fraction of ones in the data, now computed empirically. Using this cutoff, we can construct a set of candidate $x_i$ with ``large'' dot products with $x_s$, and

% For each seed, we do the following:
% \begin{enumerate}
% \item compute the dot product of this $x_s$ with all other $x_i$.
% \item sort the other $x_i$ in decreasing order of dot product
% \end{enumerate}

For each seed, we do the following:

\begin{algorithm}[h]
\caption{SelectObjectsAndFeatures}
\label{algo:selectObjectsAndFeatures}
\begin{algorithmic}[1]
% \LineComment {Do stuff, comment 1}
% \LineComment {line 2 of comment}
% \Procedure {SelectObjectsAndFeatures}{$X, x_s$}

\State {$prods \leftarrow X^\top x_s$}
% \State {$Y \leftarrow [x_i \text{ sorted by value of } P_i] $}
\State {$order \leftarrow \text{ all indices $i \ne s$ sorted by value of } prods_i $}
% \State {$\mathcal{F} \leftarrow 1,\ldots,D $}
\State {$best \leftarrow -\infty $}
\State {$\mathcal{I} \leftarrow \{s\}$; $\mathcal{F} \leftarrow \varnothing $}
\For{$i \leftarrow 1 \textbf{ to } N$}
	\LineComment {Try adding new instance}
	\State {$ \mathcal{I}^\prime \leftarrow order_i $} \COMMENT{add $i$th best candidate}

	% \LineComment {Compute the optimal $\mathcal{F}$ for $\mathcal{I}^\prime$ }
	\State {$ c \leftarrow \sum_{i \in \mathcal{I}^\prime} x_i $} \COMMENT {feature counts}
	\State {$ \theta_1 \leftarrow c \text { / } |\mathcal{I}^\prime| $} \COMMENT {feature probabilities}
	\State {$ \Delta = \log{\theta_1} - \log{\theta_0} $} \COMMENT {gap in log probs.}
	\State {$ \mathcal{F}^\prime \leftarrow \{j \text{ $|$ } \Delta_j > 0\} $} \COMMENT {optimal features}

	% \LineComment {Compute the objective given $\mathcal{F}^\prime$ and $\mathcal{I}^\prime$ }
	\State {$ score \leftarrow \sum_{j \in \mathcal{F}^\prime} c_j * \Delta_j$}
	% \State {$ \mathcal{F}^\prime \leftarrow optimalF(I^\prime) $}
	% \State {$ score \leftarrow computeObjective(\mathcal{I}^\prime, \mathcal{F}^\prime) $}
	\LineComment {Update ``best'' instances and features}
	\If {$score > best $}
		\State {$ best \leftarrow score $}
		\State {$ \mathcal{I} \leftarrow \mathcal{I}^\prime $}
		\State {$ \mathcal{F} \leftarrow \mathcal{F}^\prime $}
	\EndIf
\EndFor

\RETURN {$ \mathcal{I}, \mathcal{F} $}
% \Return {$ \mathcal{I}, \mathcal{F} $}
% \EndProcedure
\end{algorithmic}
\end{algorithm}

In words, we iterate through all objects that are not the seed object, sorted by decreasing dot product with the seed object, and test whether adding them to the set of pattern instances $\mathcal{I}$ increases the value of the objective function \ref{equation:objective}. If it does, we use this set of instances (and the associated features) as the new best. The variables $c_j$ and $\theta_{1j}$ continue to represent the counts and empirical probabilities of each feature, respectively. $\Delta$ can be seen as the gain in (log) probability from including feature $j$. $\Delta_j$ is large when the feature occurs more often in $\mathcal{I}^\prime$ than would be expected by chance.

Unfortunately, the above algorithm tends to overfit to the first several $x_i$, since these are especially similar to one another in practice. This tendency can be remedied through regularization, which we implement using two quantities.

First, we compute the odds that would be produced if we had selected the features in $\mathcal{F}^\prime$ but $\mathcal{I}^\prime$ consisted of random data:
\begin{align}
	score_{random} \equiv \sum_{j \in \mathcal{F}^\prime} \Delta_j * p_0 * |\mathcal{I}^\prime|
\end{align}
The idea is that there is a $p_0$ probability of each feature being 1 by chance, and so over the course of the $|\mathcal{I}^\prime|$ instances included, the above represents the score that would be ``accumulated'' on random data. This is effectively $L_1$ regularization of $\Delta$.

Second, we compute the score that would be produced if we had selected the features in $\mathcal{F}^\prime$ but $\mathcal{I}^\prime$ consisted not of random data, but of the next-best candidate in our ordering, $x_{next}$. Intuitively, if there is another object that looks much like the pattern, this is evidence that we do not yet have the full set of instances, and so the score should be reduced.\footnote{See the supporting document for a more rigorous interpretation.}
\begin{align}
	score_{next} \equiv \sum_{j \in \mathcal{F}^\prime} \Delta_j * x_{next,j} * |\mathcal{I}^\prime|
\end{align}

Using $score_{random}$ and $score_{next}$, we update the original score as follows:
\begin{align}
	newScore = newScore - \max(score_{random}, score_{next})
\end{align}

After incorporating this penalty, we have the complete subset selection algorithm.

% ------------------------------------------------
\subsection{Reduction for Time Series}
% ------------------------------------------------
Using the above algorithm to find patterns in time series involves three steps:
\begin{enumerate}
\item Converting each sample $t_i \in T$ into a sparse boolean vector $v_i$.
\item Extracting sliding windows of $v_i$ to form the $x_i$ on which the algorithm operates.
\item Determining the bounds of instances based on the $x_i$ selected as instances.
\end{enumerate}

% ================================
\subsubsection{Binarization}
% ================================
We construct a Boolean feature vector $v_i$ from each sample $t_i$ using a scheme inspired by edge detection in computer vision. We do not claim that this is a particularly good representation for time series---indeed, examination of the features suggests that our algorithm is good \textit{in spite}, not because of it---but it can be computed in $O(1)$ and suffices for our purposes.\footnote{We did find, however, that it was more effective than using the presence of particular SAX words [] as features, and was much more robust to baseline drift than directly quantizing the time series.}

The idea is that we will determine the slope of the signal over a sliding window centered at $i$, and set each feature in $v_i$ to 1 iff the slope falls within a particular range of values. We define the slope via the best fit of a straight line onto the window.

Formally, let $w$ be the width of a sliding window and let $y$ be a line segment of slope one, $y = 1,\ldots,w$. For now, suppose that $t_i$ is univariate. Then define:
\begin{align}
	z(i) &\equiv t_{i-w+1},...,t_{i+w} 					\\
	\Sigma_z(i) &\equiv \sum_{k=1}^{w} z_k(i) 		, \; \; \;
	\Sigma_{zz}(i) \equiv \sum_{k=1}^{w} (z_k(i))^2 	\\
	\Sigma_y &\equiv \sum_{k=1}^{w} y_k 		, \; \; \; \; \; \; \; \;
	\Sigma_{yy} \equiv \sum_{k=1}^{w} (y_k)^2 	\\
	S_{yz}(i) &\equiv \sum_{k=1}^{w} z_k(i) * y_k 	\\
	% S_{zz} &\equiv \Sigma_{zz} - (\Sigma_z)^2 / w \\
	S_{yy} &\equiv \Sigma_{yy} - (\Sigma_y)^2 / w \\
	% correlation &= S_{yz} / \sqrt(S_{zz} * S_{yy})
	slope(i) &\equiv S_{yz} / S_{yy}
\end{align}
The above equations describe a linear regression of the data onto the straight line $y$, with $slope$ being the regression coefficient.

This computation requires linear time in general, but we can reduce it to constant time per window position by exploiting the fact that most of the data remains the same at each time step. The sums and sums of squares can be update trivially be adding and removing the values that enter and leave the window, and the dot products $S_{yz}$ can be updated
as follows:
\begin{align}
	S_{yz}(i+1) = S_{yz}(i) - \Sigma_z(i) + w * t_{i+1}
\end{align}
We defer the proof to [supporting site] due to space constraints.

Note that, to avoid extreme slopes at the edges of the data, we pad each dimension with its first and last values, rather than with zeros.

Given $slope(i)$, we must determine which feature $v_i^j$ should be set to 1. Let $\delta$ be the difference between the minimum and maximum values in the data and let $J$ be the number of features desired in $v_i$. Then feature $j = \ceil{\frac{slope * w}{\delta} + J/2} $ is set to 1. If this value would exceed the bounds of the feature vector, the first or last feature is set to 1. Intuitively, this is just quantizing the slopes into $J$ buckets. We hardcode $J$ to 16 throughout the remainder of the work, since 8 was consistently too few and 32 too many. Further, we omit features $J/2$ and $J/2 + 1$ since they correspond to flat sections of data.

Using the $v_i$ at each time step, we then form the objects $x_i$ on which the algorithm will operate by concatenating the $v_i$ in a sliding window of length $M_{max}$. To generalize to multidimensional time series, we first concatenate the $v_i$ from each dimension into one larger $v_i$.

At this point, we have ignored how to set the window width parameter $w$. Our solution is to \textit{not} set it. We use multiple $w$ values, and, again, concatenate the features $v_i$ resulting from each. At the ends of the time series, where shorter windows are defined but longer ones are not, we use zero vectors in place of the features from the longer windows. The $w$ values we use are the powers of two ranging from the largest one below $M_{min} / 2$ to the largest one below $M_{max}$. Since $M_{min} \ge M_{max} / 2$, this up to three lengths. The reason for this range is that lengths much shorter than the pattern tend to pick up noise, and lengths as large as the pattern lack resolution. We use $M_{min} / 2$, rather than $M_{min}$, in case $M_{min}$ is just above a power of two.

% ================================
\subsubsection{Algorithm modifications}
% ================================

We now have the set of $x_i$ with which to run Algorithm \ref{algo:selectObjectsAndFeatures}. However, we must make two modifications to handle the time series case.

First, due to overlap constraints, we must not allow it to add two indices to $\mathcal{I}$ that are too close to one another. This can be trivially achieved by having the for loop skip to the next iteration in its first line upon encountering an $i$ that is too close to any index already in $\mathcal{I}$. We define ``too close'' as being within $M_{min}$.

Second, due to time warping and variations in instance length, a given feature may not be 1 at identical indices across two instances (Fig []). To remedy this, we ``blur'' feature values across time using a Hamming window of length $M_{min}/2$. These blurred features are placed in a separate matrix $V_{blur}$ that is used to determine the probabilities $\theta_1$. Concretely, in addition to setting $c_j = c_j + x_i$, we also set $c_{blur,j} = c_{blur,j} + x_{blur,i}$ and set $\theta_1 = c_{blur,j} / |\mathcal{I}^\prime|$, where $x_{blur,i}$ is the data in the $i$th sliding window position over $V_{blur}$. This can be seen as a form of parameter tying to incorporate the prior that features nearby in time are really the ``same'' and so should have similar probabilities. One subtlety regarding $V_{blur}$ is that after applying the filter, we must divide each value by the maximimum value within half the filter width ($M_{min}/4$) within its row so that 1 remains the highest possible value.

With these modifications, we have the complete learning algorithm.

TODO talk about learning boundaries of pattern in final filter to get start and end points

% \begin{align}
% 	v_i^j \equiv I{}
% \end{align}

% using the following equivalence. Namely, letting $t_i^{\prime} = t_{i+1}$ and $P_i$ be the dot product for the window beginning at position $i$, we have:
% \begin{align}
% 	P_i &= \sum_{k=1}^{w} t_{i+k} * y_k = \sum_{k=1}^{w} k * t_{i+k} \\
% 	P_{i+1} &= \sum_{k=1}^{w} t^\prime_{i+k} * y_k = \sum_{k=1}^{w} k * t^\prime_{i+k} \\
% 		&= \sum_{k=1}^{w} k * t_{i+1+k} \\
% 		&= w * t_{w+1} + \sum_{k=1}^{w-1} k * t_{i+1+k} \\
% 		&= w * t_{w+1} + \sum_{k=1}^{w} (k-1) * t_{i+k} \\
% 		&= w * t_{w+1} + \sum_{k=1}^{w} k * t_{i+k} - \sum_{k=1}^{w} t_{i+k} \\
% \end{align}
% In short, to update the dot product, we can subtract the sum of the elements in the previous window and add $w$ times the newly added element.



% Suppose that $t_i$ was the $k$th element of $z$ at the previous time step. Then its contribution to the dot product was $t_i * y_k = k*t_i$. At the current time step, it will be mapped to element $k-1$, so its contribution will instead be $t_i * l_{k-1} = (k-1) * t_i$. If it is no longer in the window,


% The function $optimalF(\mathcal{I})$ can be computed via the following equations:
% \begin{align}
%  	% k &= |\mathcal{I}| \\
%  	% c_j &= \sum_{i \in \mathcal{I} x_i^j} \\
% 	\theta_{1j} &= c_j / k \\
% 	\Delta_j &= \log{\theta_{1j}} - \log{\theta_{0j}} \\
% 	% odds_{j} = c_j * \Delta_j \\
% 	\mathcal{F} &= \{j | \Delta_j > 0\}
% \end{align}

% where $c_j = \sum_{i \in \mathcal{I} x_i^j}$ and $k = |\mathcal{I}|$ as before.


% Concretely, we can evaluate the objective function using the $\mathcal{I}$ and $\mathcal{F}$ associated with each and take the best.

% It is unlikely that this would match up so well with either of $x_1$ or $x_2$ by chance, and since we already suspect that there is a pattern of roughly this form, we can be even more certain of the pattern's existence.



% As it turns out, we do not even need the oracle. If we \textit{ever} encounter two examples that are this similar, we should suspect that they are two instances of some pattern (at least in a reasonably small dataset).












