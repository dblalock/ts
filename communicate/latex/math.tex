
\documentclass[conference]{IEEEtran}
\input{setup.tex}
\begin{document}

% ================================================================
\title{Some Math}
% ================================================================

% author names and affiliations
% use a multiple column layout for up to three different
% affiliations
% \author{\IEEEauthorblockN{Davis W. Blalock and John V. Guttag}
% \IEEEauthorblockA{
% Computer Science and Artificial Intelligence Laboratory \\
% Massachussetts Institute of Technology\\
% Cambridge, MA, 02139\\
% \{dblalock, guttag\}@mit.edu}
% }

\maketitle % make the title area

% ================================================================
% \begin{abstract}
% ================================================================

Behold, some math.

% \end{abstract}

% ================================================================
% \section{The Robbers and Randos Problem}
\section{The Robbers and Randoms Problem}
% ================================================================

% ================================================================
\subsection{Intuition}
% ================================================================

% Your institution has been infiltrated by spies. It's up to you to determine who is a spy is and who is an innocent civilian. You know that the spies all have something in common (perhaps a former employer, for example), but you don't

A gang of robbers has stolen something from your institution. Police have identified a group of suspects, but this group contains both robbers and random civilians (drawn iid from the general population). As the local data scientist, it's up to you to determine who the robbers are.

\textit{A priori}, this is not necessarily solvable. If the robbers have nothing in particular that distinguishes them, then there can be little hope of identifying them. However, it is probable that fellow gang members will have some sort of distinguishing features. For example, if several of the suspects all happened to be from the same neighborhood, it is unlikely that this would happen in iid civilians, and so you might suspect that these are the robbers. If you can determine what these features are, then, you can recognize the robbers. So how could you identify these features? The above example seems sensible enough, but what is it about a given feature that makes it useful?

One rule is that the feature must be \textit{distinguishing}---i.e., not common in the general populace. ``Has brown hair'' is probably a poor feature.

However, it is not enough for a feature to be unusual. Indeed, any person in the group is likely to have a number of features that are not just unusual, but unique. We cannot simply lock up, say, the one redhead.

However, if multiple people have the \textit{same} unusual feature, we might suspect that this is not a coincidence. For example, if three of our suspects happen to be from the same neighborhood, or work for a particular company, this is unlikely under the assumption that they're innocents drawn from the general population. This becomes even more unlikely the more suspects there are who have this trait in common.

Further, if this same set of individuals has other features in common (e.g., working for the same company, being the same age), this bolsters our conviction.

In short, we seek features with three properties:
\begin{enumerate}
\item \textbf{rare} in general
\item \textbf{common} in the suspected robbers
\item shared across \textbf{many} robbers
\end{enumerate}

% There is an outbreak of a mysterious illness in your town. We'd like to learn what distinguishes the people who are susceptible to it (and which people they are). We have a room full of people, some of whom we know are suseptible disease.

% People are carriers for the disease for some time before the symptoms manifest themselves, and we'd like to determine which people are carriers. Further, we'd like to identify what distinguishes these people, so that we can identify other carriers in the future. Unfortunately, we have no labeled examples of carriers or non-carriers, because we can only tell someone was a carrier once they have symptoms, at which point they're We've identified a room full of people that we know contains a few carriers, but many random non-carriers as well.

% ================================================================
\subsection{Objective Function}
% ================================================================

% TODO I should do argmax_I max_F throughout everything; that makes more sense cuz doesn't assume that we care about the features for their own sake

Formally, you are given a set $X$ of feature descriptions of individuals, $x^{(i)}$, where $x^{(i)}_j$ is the $jth$ feature of the $ith$ individual. Your task is to return the most probable set of robbers, $I^{\ast}$, and distinguishing features, $F^{\ast}$. There are $N$ individuals, at least two robbers ($|I| \ge 2$) and $D = |x^{(i)}|$ features. Your objective function is defined as follows:

\begin{align} \label{eq:abstractObjFunc}
	I^{\ast}, F^{\ast} = \argmax_{I, F} \prod_{i \in I} \prod_{j \in F} \frac{p(x^{(i)}_j | z_i = 1)}{p(x^{(i)}_j | z_i = 0)}
\end{align}

where $z$ is an indicator variable describing whether $i \in I$, and different distributions are associated with each value of $z$. In words, we want to find a set of individuals and features such that the probability of getting all of those features across all of those individuals is much higher if the individuals are pulled from the ``robber'' distribution than the ``random'' distribution. We will describe the distributions we use in solving our particular problem in following sections, but all that is required for the above objective is that the distributions be different.

% ================================================================
\subsection{Objective Function Derivation}
% ================================================================

In this section, we demonstrate that the above objective function yields the maximum \textit{a posteriori} estimate of the set of robbers and their distinguishing features given a few simple assumptions.

By definition, the most probable set of objects and and features is given by:
\begin{align}
	\argmax_{I, F} p(I, F | X)
\end{align}
Using Bayes Theorem and dropping the denominator (since it does not affect the argmax), we see that this is equivalent to:
\begin{align}
	\argmax_{I, F} p(X | I, F)p(I, F)
\end{align}
Now suppose that our priors regarding the set of objects and set of features are independent. We then have:
\begin{align}
	\argmax_{I, F} p(X | I, F)p(I)p(F)
\end{align}
The first of these probabilities, $p(X | I, F)$ can be simplified further given a few reasonable assumptions. First, since we consider each object to have been drawn iid, we have:
\begin{align}
	p(X | I, F) = \prod_{i} p(x^{(i)} | I, F)
\end{align}
Now, we introduce the latent indicator variable $z$, where $z_i = 1 \iff i \in I$, to encapsulate the conditioning of $x^{(i)}$ on $I$. This allows us to rewrite the previous equation as:
\begin{align}
	p(X | I, F) &= \prod_{i \in I} p(x^{(i)} | F, z=1) \prod_{i \notin I} p(x^{(i)} | F, z=0) \\
	&= \prod_{i \in I} \frac{p(x^{(i)} | F, z=1)}{p(x^{(i)} | F, z=0)} \prod_{i} p(x^{(i)} | F, z=0)
\end{align}
where we have multiplied in $\prod_{i \in I} p(x^{(i)} | F, z=0)$ to get the second equation. Now let us define $p(x^{(i)} | F, z=0) = p(x^{(i)} | z=0)$, reflecting the idea that the probability of getting $x^{(i)}$ from the ``random'' distribution is not dependent on which features we select as relevant for the ``robber'' distribution. This renders the second product independent of both $I$ and $F$, allowing us to drop it without affecting our original argmax:
\begin{align} \label{eq:beforeFactorF}
	p(X | I, F) \propto \prod_{i \in I} \frac{p(x^{(i)} | F, z=1)}{p(x^{(i)} | z=0)}
\end{align}
We can reasonably make another assumption to simplify this even further. If only the features $j \in F$ are determined by an object's status as a robber, and others are determined by whatever processes produce the features of the general population, then one could reasonably factor the overall probability of an object into the probability of the robber and non-robber features. That is, we can define:
\begin{align}
 p(x | F, z) \equiv p(x_F | z)p(x_{-F} | z) \\
 p(x_{-F}, z = 1) \equiv p(x_{-F} | z = 0)
\end{align}
which yields:
\begin{align} \label{eq:afterFactorF}
	p(X | I, F) &\propto \prod_{i \in I} \frac{p(x^{(i)}_F | z=1)}{p(x^{(i)}_F | z=0)} \frac{p(x^{(i)}_{-F} | z=1)}{p(x^{(i)}_{-F} | z=0)} \\
	&= \prod_{i \in I} \frac{p(x^{(i)}_F | z=1)}{p(x^{(i)}_F | z=0)}
\end{align}
where we have vacuously conditioned on $F$ in the denominator of \ref{eq:beforeFactorF} in order to obtain \ref{eq:afterFactorF}.

Now consider what happens if we assume that $p(x^{(i)}_F | z)$ factorizes---i.e., that the individual features are independent when conditioned on $z$. This gives us:
\begin{align}
	p(X | I, F) \propto \prod_{i \in I} \prod_{j \in F} \frac{p(x^{(i)}_j | z=1)}{p(x^{(i)}_j | z=0)}
\end{align}
This yields the final objective function:
\begin{align}
	I^{\ast}, F^{\ast} = \argmax_{I, F} p(I)p(F)\prod_{i \in I} \prod_{j \in F} \frac{p(x^{(i)}_j | z=1)}{p(x^{(i)}_j | z=0)}
\end{align}
If we assume uniform priors $p(I)$ and $p(F)$, so that these terms do not affect the argmax, we recover the original objective function in \ref{eq:abstractObjFunc}.

% ================================================================
\subsection{Binary Case}
% ================================================================

The previous sections demonstrated the utility of our abstract objective function in locating objects and features of interest. In this section, we instantiate this objective with concrete distributions.

Specifically, we consider the case wherein all of the features are boolean-valued and drawn from Bernoulli distributions. We define $(p(x^{(i)}_j | z=0) \sim$ Bernoulli($\theta_{0j}$) and $(p(x^{(i)}_j | z=1) \sim$ Bernoulli($\theta_{1j}$), where $\vec{\theta_0}$ is any set of parameters computed independent of $I$ and $F$, and $\vec{\theta_1}$ is the optimal set of parameters (in this case, simply the maximum likelihood estimate) for $\{x^{(i)}_j | i \in I\}$. We also drop $p(I)$ for now---it will be relevant when we generalize to time series. Taken together, these changes give the objective function:
\begin{align}
	I^{\ast}, F^{\ast} = \argmax_{I, F} p(F)\prod_{i \in I} \prod_{j \in F} \frac{p(x^{(i)}_j | \theta_{1j})}{p(x^{(i)}_j | \theta_{0j})}
\end{align}
Expanding the two cases for each feature, we have:
\begin{align}
	\argmax_{I, F} p(F)\prod_{i \in I} \prod_{j \in F} \frac{\theta_{1j}^{I\{x^{(i)}_j = 1\}}(1 - \theta_{1j})^{I\{x^{(i)}_j = 0\}}}{\theta_{0j}^{I\{x^{(i)}_j = 1\}}(1 - \theta_{0j})^{I\{x^{(i)}_j = 0\}}}
\end{align}
Letting $c_j$ denote the number of times feature $j$ is 1, ${\sum_{i \in I} x^{(i)}_j}$, and $k$ denote $|I|$, this can be rewritten as:
\begin{align}
	\argmax_{I, F} p(F)\prod_{j \in F} \frac{\theta_{1j}^{c_j}(1 - \theta_{1j})^{(k - c_j)}}{\theta_{0j}^{c_j}(1 - {\theta_0j})^{(k - c_j)}}
\end{align}
Now, let us take the log of this objective function (which will of course yield the same answer, since the log is a monotonic function of its argument):
\begin{align}
	\argmax_{I, F} p(F)\sum_{j \in F} [c_j (log(\theta_{1j}) - log(\theta_{0j})) \\
	+ (k-c_j)(log(1-\theta_{1j}) - log(1-\theta_{0j})) ]
\end{align}
% TODO actually this is false cuz of bayesian occam's razor; could get a tighter theta_1 if we exclude some stuff
Under this objective, it appears that there is no reason ever to exclude any $j$ from $F$, since $\theta_{1j}$ can always be fit to yield at least as large a value as $\theta_{0j}$. Fortunately, this can be addressed via the heretofore neglected $p(F)$. Specifically, define:
\begin{align}
	p(F) \equiv \lambda e^{-\lambda|F|} \lambda > 0
\end{align}
This gives the regularized log objective:
\begin{align}
	\argmax_{I, F} \sum_{j \in F} [c_j (log(\theta_{1j}) - log(\theta_{0j})) \\
	+ (k-c_j)(log(1-\theta_{1j}) - log(1-\theta_{0j})) - \lambda]
\end{align}
for some $\lambda > 0$. Intuitively, this means that the difference in (log) probabilities of a feature $j$ happening as many times as it does under the ``robber'' distribution must be at least $\lambda$ greater than it is under the ``random'' distribution. Consequently, we will only include features that are quite likely to be indicative of being a robber.

Another simplification is possible if the data is sparse. In this case, $\theta_{0j}$ and $\theta_{1j}$ are both $\ll 1$, and thus $log(1 - \theta_{0j}) \approx log(1 - \theta_{1j}) \approx 0$. This allows us to closely approximate the previous equation without the second term:
\begin{align}
	\argmax_{I, F} \sum_{j \in F} [c_j (log(\theta_{1j}) - log(\theta_{0j})) - \lambda]
\end{align}

% ================================================================
\subsection{Setting $\lambda$}
% ================================================================

At this point, we have a free hyperparameter, $\lambda$, which determines how selective we are when deciding which features to include in $F$. Fortunately, there is a simple way to set this parameter based on the data.

Suppose that there are no robbers in the data, and each $x^{(i)}_j \sim $ Bernoulli($\theta_{0j}$), where $\theta_{0j} = p(x^{(i)}_j) = E[x^{(i)}_j]$. Consider $k$ objects suspected of being robbers. On average, there will be $c$ ones in each of these, where $c = \sum_{j} \theta_{0j}$. This menas the expected fraction of ones in each will be $c / D$, where $D$ is the number of features. Since the ones are selected independently in each object, the expected fraction of features that are one in all of them is $(c/D)^k$, and the total number of such ones is $D(c/D)^k$.

Since all of these ones were generated at random, $\lambda$ ought to be set such that none of these ``false positive'' features are selected. Fortunately, the number of false positives diminishes geometrically as $k$ increases (and particularly quickly since we have previously assumed sparsity, $c \ll D$), so it is reasonable to focus on the case where $k=2$.

When $k=2$, the features $j$ that are one in both objects will have $\theta_{1j} = 1$ (since this is the best fit), so $c_j (log\theta_{1j} - log\theta_{0j}) = -2log\theta_{0j}$. One could have a separate $\lambda_j$ that was  greater than this value for each $j$ separately, but it is simpler to just use a single average probability, $\frac{1}{D}\sum_j \theta_{0j}$, which is the approach taken throughout this work.

In short, we set each $\theta_{0j}$ to the emprical probability that $x^{(i)}_j = 1$, compute the average probability $p_0$ over all features, and then set $\lambda = -2log(p_0)$.

% TODO I'm likely to actually drop the scaling by 2 seems it seems to not work as well; in this case, we simply have that putting a pattern there simply has to increase the likelihood more than the cost of encoding the pattern (at least in the compression case)

% ================================================================
\subsection{Relationship to compression}
% ================================================================

It is interesting to note that the binary objective connects elegantly to the problem of compression. Suppose we drop the $p(F)$ $p(I)$ terms, and that $\vec{\theta_0}$ is set to the emprical probabilities across the whole dataset, so that $\theta_{0j} = p(x_j)$. If we are forced to select an $I$ such that $c_j = k$, and thus $\vec{\theta_1} = \vec{1}$, this becomes:
\begin{align}
	& \argmax_{I, F} \sum_{j \in F} -k * log(\theta_{0j}) \\
	=& \argmax_{I, F} \sum_{j \in F} -k * log(p(x_j))
\end{align}
This is simply the number of bits to encode $k$ occurrences of feature $j$, (assuming that its absence must also be encoded elsewhere). In other words, if each feature $j$ would otherwise be encoded at a cost of $log(p(x_j))$ when present, this is the number of bits saved by substituting a symbol composed of ones at the appropriate indices (see Fig TODO). Thus, this objective is maximized when we find the largest set of ones that occur together the most times throughout the data. This paragraph is confusing.

% ================================================================
\subsection{Solution}
% ================================================================
Given sparse binary features, we demonstrated in the previous sections that the most probable set of robbers, $I^{\ast}$, and distinguishing features, $F^{\ast}$, can be obtained as:
\begin{align}
	I^{\ast}, F^{\ast} = \argmax_{I, F} \sum_{j \in F} [c_j (log(\theta_{1j}) - log(\theta_{0j})) - \lambda]
\end{align}
where $\lambda = -2log(p_0)$ and $p_0$ is the average probability of a feature being 1. In this section, we describe a fast algorithm for finding an approximate solution.

The basic idea is that we just use the MK algorithm to get the closest pair, and then generalize to other instances from there. The sneaky part is that we don't just compare to info gain over $\vec{\theta_0}$, but also to the next best object we haven't included yet.

% ================================================================
\subsection{How this is a filter}
% ================================================================

We set $\vec{w} = V(log\vec{\theta_1} - log\vec{\theta_0})$, where $V$ is a binary diagonal matrix that zeros out all the features that aren't included in the pattern. Then $\vec{w}^\top \vec{x}$ is the difference in log probabilities of getting $\vec{x}$ under the ``pattern'' distribution and ``non-pattern'' distribution, which is also the log odds of it being a pattern instance. To incorporate $\lambda$, we subtract $\lambda |F|$ from this dot product.

% ================================================================
% References
% ================================================================

% \IEEEtriggeratref{27}	% trigger column break to make cols even

% references section
\bibliographystyle{IEEEtran}
% argument is your BibTeX string definitions and bibliography database(s)
\bibliography{IEEEabrv,doc}

\end{document}
