================================================================
Overall pseudocode outline thing
================================================================

Transform the time series $T$ into a feature matrix $\Phi$
-select subsequences from the data to use as shape features
-transform $T$ into $\Phi$ by encoding the presence of these shapes across time
-blur $\Phi$ into $\tilde{\Phi}$ to achieve length and time warping invariance

Generate sets of windows that likely contain instances
-find ``seed'' windows that are unlikely to have arisen by chance
-find ``candidate'' windows that resemble each seed window
-rank these candidates based on similarity to their seeds

Estimate the true instances within these candidates
-greedily construct subsets of candidate windows based on ranks
-score these subsets and select the best
-infer exact instance boundaries within these best windows

================================================================
Better FindInstances Pseudocode
================================================================

FindInstances(S, X) // seed windows, all windows
\mathcal{I}_{best} = {}
score_{best}
For each seed s in S:
	P <- \{s \cdot x_i, x_i \in X\} // dot products
	C <- spacedRelativeMaxima(P, M_{min})	// candidate window indices
	C <- sortIdxsByDescendingDotProd(C, P) // ranked candidates
	for i \leftarrow 2 to |C|:
		\mathcal{I} = \{c_1,...,c_i\} // greedy subset of C
		score = computeScore(\mathcal{I}, c_{i+1})
		if score > score_{best}:
			score_{best} = score
			\mathcal{I}_{best} = \mathcal{I}


================================================================
All of the reasons I'm better
================================================================

relaxes a ton of assumptions:
	-no time warping
	-known pattern length
	-fixed instance length
	-univariate, all dims correlated, all dims relevant
	-only need 2 instances

allows operating on multiple transforms of the data / doesn't force everything to just be shape comparisons


================================================================
Random
================================================================


stuff that should be in 1st page:
	-define application
	-argue it's important
	-explain why hard / unsolved
	-intuition behind key insight

------------------------

As sensors of all sorts proliferate, the time series data that they produce is becoming increasingly common. At present, it is easy to extract value from this data when it has an obvious meaning (e.g., a room's temperature), but extremely challenging when it it does not (e.g., the acceleration signal of a smart watch).

------------------------

-So what we have is the ability to get lots of raw data reflecting what some physical system is doing
-What we want is the ability to tell what's happening in that system
-

As sensors proliferate with the rise of wearable technology and the internet of things, the time series data that they produce is becoming increasingly ubiquitous. In many cases, however, it is not the raw data itself that is desired, but the picture it offers of the underlying system being sensed.

------------------------

Outcasts: Accurate Pattern Discovery in Multivariate Time Series

The rise of wearable technology and the internet of things have enabled collection of data on human health, industrial operations, and numerous other domains at a granularity previously unimaginable. However, extracting value from this data can be challenging, since sensors report only low-level signals (e.g., acceleration), not the presence of high-level phenomena of interest (e.g., eating). We introduce a technique to bridge this gap by automatically learning what real-world events ``look like'' in the data with no human labeling.

By finding sections of data that are suspiciously similar to one another and different from all others, we learn to recognize such diverse phenomena as human actions, power consumption patterns, and spoken words. In addition to being much more accurate than alternatives, our algorithm is also an order of magnitude faster. Further, our evaluation is the most rigorous ever conducted for this task, involving 100-fold more ground truth than other works and the manual labeling of over 1 million data points.

------------------------

The rise of wearable technology and the internet of things have made available vast quantities of sensor data, and with them the promise of insights into domains ranging from human health to user interfaces to manufacturing.

the correspondence between low-level signals and the real-world phenomena generating them is often non-obvious. We introduce a technique to learn these correspondences---i.e., what real-world events "look like" when reflected in the data---from raw signals without costly human labeling.

Why we're cool:
	-we're just straight up more accurate
	-we're faster
	-we don't assume:
		-univariate
		-all dims relevant (or worse, correlated)
		-only 2 instances
		-all instances same length (or worse, that this length is known)

------------------------

The rise of wearable technology and the internet of things have made available a vast amount of sensor data, and with it the promise of improvements in everything from human health [] to user interfaces [] to agriculture []. Unfortunately, the raw sequences of numbers comprising this data are often insufficient to offer value---a smart watch user is not interested in their arm's acceleration signal, for example. Instead, it is higher-level phenomena reflected in this data, such as taking steps, that is of interest.

Recognizing these high-level phenomena using low-level signals is currently problematic. Given enough labeled examples of the phenomenon taking place, one could in principle train a classifer for this purpose. Unfortunately, as Minnen notes [] ``words about how extracting the examples is much harder than doing the recognition''. This problem is worsened by the uninterpretability of time series data; while humans can easily label images or text, labeling a sequence of numbers can be difficult or impossible. Moreover, one may not even know that a pattern exists to be labeled---indeed, much of the literature on mining time series focuses on pattern *discovery* [][][][][][][].

To overcome these difficulties, we introduce an algorithm that discovers occurrences of phenomena within unlabeled time series. Specifically, given a time series in which certain time intervals contain some characteristic but unknown pattern (reflecting a particular phenomenon), our algorithm returns these intervals and a learned model of the pattern. Using our technique, one can automatically learn to recognize gestures, power consumption patterns, and isolated words given only a section of data containing several examples thereof.

As an illustration of our problem, consider Fig 1, which depicts the current, voltage, and other power measures of a home dishwasher over time (TODO human actions instead). Shown are three instances of the dishwasher running, with idleness in between. With no prior knowledge or domain-specific tuning, our algorithm correctly determines not only what this repeating pattern looks like, but also where precisely it begins and ends.

Our technique is based on two insights:
1) ``Pattern'' regions ought to both look similar to one another, and different from everything else.
2) If ``similar'' and ``different'' are defined based on local features, rather than traditional global distance comparisons, one can find these regions without prior knowledge of the pattern's length.

The remainder of the paper is organized as follows. In section [], we formally describe our problem and why it is challenging. In section [], we explain how our problem and approach differ from related work. In sections [] to [], we give the intuition for how and why our technique works, and then describe it in detail. In sections [] and [], we evaluate it on a battery of real and synthetic datasets and discuss the implications of our results.





An ideal pattern discovery algorithm would take in this raw data and return these three instances, perhaps as pairs of start and end indices.

Unfortunately, even when told the exact length of this pattern and how many instances of it there are, the current state-of-the-art fails to find these indices, picking up on the high similarity of ``idleness followed by the initial phases of the wash cycle'' and ignoring the suspicious coincidence that the data just after this pattern is also quite similar.


Like other work, we assume that there are at least two such intervals. Unlike other work, we operate on multivariate time series and make minimal assumptions regarding the pattern's distinguishing features, its duration, how many times it occurs, or what subset of variables it affects. Further, our approach is both much faster and much more accurate on real-world data.

Our advances are enabled by two key insights:
1) It is not sufficient to consider only how similar regions of data are when assessing whether they are instances of a pattern; one must also consider how different they are from other regions
2) Similarity based on local features, rather than global ones, allows us to learn what patterns look like without precise knowledge of where their instances begin and end.



Bridging this gap between low-level signals and high-level phenomena is currently problematic.

Like many related works [], we m


Because of the cost or impossibility of humans manually locating phenomena of interest within time series, it is desirable to discover them algorithmically.




However, it is often not the raw sequences of numbers comprising this data that are of interest---instead, it is the real-world phenomena reflected in them that are of interest. A gestural interface, for example, does not need to know the raw accelerations describing a user's movement, but instead what gesture the user is performing.

it is rarely the raw data itself that is of interest, but instead the real-world phenomena that this data reflects.

================================================================
Related work
================================================================

Lotsa related work

motifs (similar problem)
	-most similar subseqs
		-discrete stuff
		-pair
			-often just wants closest pair, perhaps to feed to a domain expert
	-same problem (closed or maximal motifs)
		-MOEN
		-MDL
		-Minnen
			-but mostly postproc and HMM requires similar offset + amplitude; also requires length and number of neighbors for density estimate
		-all of these approaches rely on seeding with lowest-distance pairs (or collections), which we show to be inaccurate

shapelets / local features (similar in approach)
	-learned shapelets + BOSS + shotgun outperforming DTW
		-but used for classification
	-u-shapelets good at identifying representative stuff in ts
		-similar, but only assume entire ts is atomic; ie, cannot tell us that it really contains two different instances of something

Our problem is most similar to work on motif discovery, and our approach is most similar to work on shapelet learning.

A great deal of work has been done on pattern discovery in time series. Most commonly, this work focuses on finding ``motifs'', which are regions of maximal similarity under some distance measure. In most cases, the problem is to locate only the closest pair for subsequent clustering [][] or human inspection [][][], although this is commonly repeated many times [][][]. This can be done either in the time domain, or f

Finding repeating patterns (typically termed ``motifs'' []) is a commonly studied problem in time series data mining. Most motif discovery algorithms use one of two approaches: discretization and closest-pair search. The former techniques either quantize [] the time series or use a technique such as Symbolic Aggregate Approximation [] to convert subsequences to symbols [][][]. This yields a string representation which can be mined using tools such as suffix trees [][] or grammar induction [][]. While this approach is fast, it often has trouble finding the full extents of patterns []. Further, generalizing it to multivariate data is challenging since more dimensions make it less likely for similar regions to yield identical strings.

The closest-pair approach is more common. It typically defines ``closeness'' in terms of Euclidean distance and formulates the task as a search problem. There are both fast approximate [] and exact [][] techniques to find this pair, although the latter generalize to multivariate data only when distance is defined by summing over all dimensions. This problem formulation has demonstrated utility as a subroutine in clustering [][] and classification [] as well as for knowledge discovery by human experts [][]. However, generalizing from the closest pair to all instances as we desire is difficult. This is the entire focus of [], which uses an entropy measure to determine which subsequences are similar enough to be considered instances. Further, as we show experimentally, the closest pair is often not a pair of pattern instances to begin with, rendering any such generalization efforts hopeless.

Orthogonal to both of these approaches are several techniques to refine motif results after-the-fact. These refinements include updating the lengths of the motifs returned [][] or combining different motifs (often those found at different search lengths).



In terms of approach, our work is inspired by others using local features. Most notably, several of the most accurate time series classifiers\footnote{Note that [] and [] also significantly outperform the gold standard of 1NN DTW.} [][][][] work by transforming the data based on distances to representative sequences known as ``shapelets'' []. The only classifier that outperforms them\footnote{(to the best of our knowledge and excluding ensemble methods)} [] also uses local features. Further, shapelets have demonstrated utility in clustering [][], although this work treats entire time series, rather than certain regions thereof, as the objects to be clustered. These works do not share our problem formulation, but their results make clear that time series can fruitfully be represented by local features.


================================================================
Shapelets version
================================================================

Accurate Time Series Motif Discovery Using Shapelets

Finding repeating patterns, typically called motifs, is a foundational task in mining time series. Most existing work has focused on univariate time series and either assumes prior knowledge of the motif's length or requires a costly search over multiple lengths.

We propose an algorithm that discovers motifs of unknown length in multivariate time series. Our insight is that similar regions of data will tend to have subsequences that are more similar in shape than would be expected by chance. In addition to being much more accurate than alternatives, our algorithm is also an order of magnitude faster. Further, our evaluation is the most rigorous ever conducted for this task, involving 100-fold more ground truth than other works and the manual labeling of over 1 million data points.


================================================================
Intro story, v3
================================================================

------------------------ section{Intro}

same intro stuff about wanting to find instances of a pattern

new justification of it being hard:
	"labeled samples are often time consuming and expensive to get, as they require huge effort from human annotators or experts." -NuActiv paper

insights:
	-A pattern can be described by some characteristic set of features. We can therefore find the pattern by finding sets of features that repeatedly occur together.
	-Under this formulation, irrelevant dimensions and non-pattern sections of data produce features that are not in this set; we can therefore learn to ignore both by selecting the correct set of features.
	-If features are sparse, we can find the correct set with high probability after only a few examples.

contributions:
	-formulation of pattern discovery in the multivariate case when instance lengths, positions, and relevant dimensions are all unknown. Also allows categorical variables. Plus extensive experiments showing that this formulation corresponds well to patterns in real data.
	-an efficient algo to solve this formulation; this formulation can also handle variations in instance lengths and time warping
	-a large body of labeled, publicly available data, as well as open-source code to evaluate algorithms on it.


------------------------ section{Definitions and Problem Statement}

current <defs>
	-maybe def of pattern also?

problem statement (feature flock objective func)
	-then mention that we have experiments showing that this corresponds to ground truth regions on craptons of datasets

assumptions: // about the problem
	-what we don't assume
	-what we do assume (shortened)

------------------------ section{Related Work}

maybe put in stronger wording about how our assumptions are more relaxed than existing stuff

---- then everything else




