To formalize our task, we introduce the following definitions.

\begin{Definition}{
\textbf{Time Series}. A $D$-dimensional time series $T$ of length $N$ is a sequence of real-valued vectors $t_1,\ldots,t_N, t_i \in \mathbb{R}^D$. If $D = 1$, we call $T$ ``univariate'' or ``one-dimensional'', and if $D > 1$, we call it ``multivariate'' or ``multi-dimensional''.
}
\end{Definition}

% \begin{Definition}{\textbf{Position}. A position $i, 1 \le N$ in a time series $T$ of length $N$ is an index into $T$.
% }
% \end{Definition}

\begin{Definition}{\textbf{Region}. A region $R$ is a pair of positions $(i, j), i < j$. The value $j - i + 1$ is termed the \textbf{length} of the region, and the time series $t_i,\ldots,t_j$ is the \textbf{subsequence} for that region. If a region reflects an occurrence of the pattern, we term it a pattern \textbf{instance}.
}
\end{Definition}

% \begin{Definition}{\textbf{Subsequence}. A subsequence $S[i : j]$ of a time series $T$ is the time series $t_i,\ldots,t_j$. It corresponds to the data at a particular region $(i, j)$.
% }
% \end{Definition}

% \begin{Definition}{\textbf{Instance}. An instance is a region in some privileged set $\mathcal{L}$ reflecting where the pattern has happened.
% }
% \end{Definition}

% \begin{Definition}{\textbf{Pattern}. A pattern is some set of characteristics in a time series corresponding to the occurence of some latent phenomenon of interest. A pattern is said to “happen” at each instance.
% }
% \end{Definition}

% ------------------------------------------------
\subsection{Assumptions}
% ------------------------------------------------

Using these definitions, our assumptions can be stated as follows.
% Since one of our contributions is relaxing many assumptions made by existing work, we enumerate them in some detail.
\begin{itemize}
\itemsep0em
\item There exists a latent set of true instances $\mathcal{R}^{\ast}$ in which the pattern has truly taken place.
\item These instances are more similar to one another (for some notion of similarity) than would be expected by chance.
\item There is exactly one pattern in the data. While real data often reflects multiple distinct patterns, we find that existing algorithms struggle on even this simplified problem under our formulation, and so confine the present work to the one pattern case.
% I.e., we cannot hope to discover the pattern if it has no effect on the data or perturbs it completely unpredictably.
\item There are at least two instances; i.e., $|\mathcal{R}^{\ast}| \ge 2$.
\item The instances do not overlap in time.
\item There exist bounds $M_{min}$ and $M_{max}$, $M_{min} \ge M_{max}/2$ on the lengths of instances. Some other works do not assume such bounds, but our experience is that they are easily determined via cursory inspection of the data or knowledge of the application, and that assuming no such bounds is unnecessarily complicating the problem.
% Even if these bounds could not be determined and one wanted to brute force search all possible pattern lengths, the fact that one bound is a multiple of the other means that this would require only $O(log(N))$ runs for a length $N$ time series.
\end{itemize}

\hspace{-10pt}We do not assume knowledge of: % no indent
\begin{itemize}
\itemsep0em
\item What characteristics distinguish the pattern or hold across its instances. In particular, we do not assume that all instances have the same mean and variance, so we cannot bypass the standard step of normalization when making similarity comparisons.
\item The precise number of times the pattern happens.
% , except that this number is small; i.e., we do not address the (easier) case of dozens or hundreds of instances.
\item How far apart instances of the pattern are.
\item The lengths of instances, except that they are within the aforementioned bounds. In particular, we do not assume that there is a fixed length shared by all instances.
\item Which dimensions are affected by the pattern.
\item Anything about the dimensions that the pattern does not affect.
\end{itemize}

% ------------------------------------------------
\subsection{Problem Statement}
% ------------------------------------------------
\textit{Let $T$ be a time series with latent pattern instances $\mathcal{R}^{\ast}$. Given only $T$ and the lower and upper length bounds $M_{min}$ and $M_{max}$, return $\mathcal{R}^{\ast}$.}
\vspace{.5em}

Note that, since the subsequences in $\mathcal{R}^{\ast}$ are not distinguished by any universal mathematical property, no algorithm can be guaranteed to return the right answer. However, with the above assumptions ruling out the cases wherein there are many patterns (and thus which one should be returned is ill-defined) or the pattern instances have nothing in common (in which no algorithm could reasonably hope to identify them), this problem can almost always be solved on real data.

The idea is that we can solve a proxy problem whose answer corresponds closely to $\mathcal{R}^{\ast}$ in practice. For example, one might find the most similar subsequences under the Euclidean distance [] or those that afford the best compression of the data []. Our objective function is described in \S \ref{section:method}.
%Letting $\mathcal{X} = \{x_1,\ldots,x_N\}$ be the set of all regions, $\theta_1$ be the learned ``pattern'' model parameters, and $\theta_0$ be the ``non-pattern'' model parameters, our proxy problem is:
% \begin{align} \label{eq:outcastObjFunc}
% 	\mathcal{R}^{\ast}= \argmax_{\mathcal{R}} p(\mathcal{R}) \prod_{i \in \mathcal{R}} \frac{p(x_i | \theta_1)}{p(x_i | \theta_0)}
% \end{align}

%In words, this says that we seek a set of regions that are better explained by a custom ``pattern'' model than a default ``non-pattern'' model, and the more such regions we find the better. Since the pattern model will depend on the data in $\mathcal{R}$, this encourages finding regions that are similar to one another, so that the same model can assign them all high probability.

%Mathematically, this objective is the odds of each region in $\mathcal{R}$ being an instance of the pattern, with equal priors on $\theta_1$ and $\theta_0$. % SELF: not just factored out cuz then would be raised to power of |R|

% This objective could be maximized for any number of models $p(x_i | \theta)$, but we focus on a simple one operating on binary features (Section \ref{section:method})

% This objective could be maximized for any number of models $p(x_i | \theta)$, but we focus on a specific and simple one throughout the remainder of this work. We defer a full description to Section [], but the idea is to use binary features that encode the presence of particular shapelets [] in each region.

% ------------------------------------------------
\subsection{Why the task is difficult}
% ------------------------------------------------

Even when solving an appropriate proxy problem, the aforementioned lack of assumptions means that the number of possible sets of regions and relevant dimensions is intractably large.

Suppose that we have a $D$-dimensional time series $T$ of length $N$ and $M_{min} \le M \le M_{max}$. There are up to $O(N/M)$ instances, since this is what fits in $T$ without overlap. These instances can collectively start at (very roughly) $\binom{N}{N/M}$ positions. Further, each can be of $O(M)$ different lengths. Finally, the pattern may affect any of $2^D - 1$ possible subsets of dimensions. Altogether, this means that there are roughly $O(N^{N/M} * M^{N/M} * 2^D)$ combinations of positions, lengths, and dimensions one might check.

Worse, assessing the wrong combination may give us little or no information with which to move forward. If we look for the pattern in the wrong dimensions or in a set of regions in which it does not occur, we could easily miss it or mistakenly hone in on a false pattern. Further, even if we include many of the right regions and dimensions, dissimilarities in the other regions and dimensions can drown out the pattern and cause us to miss it. In short, it \textit{appears} that solving this problem amounts to an intractable search task.

% Another obstacle is the need for normalization when comparing different regions. Because time series are often subject to baseline drift and amplitude variations, there is consensus that it is essential to normalize them based on their mean and variance before making similarity comparisons [][][]. Unfortunately, this normalization means that even two subsequences containing mostly identical data can look extremely different [], since the remaining data could arbitrarily alter their overall means and variances. A consequence of this is that, if the pattern is of length $M$, and we do not search at a length close to $M$, we may miss it \textit{even when examining subsequences where it happened}. This implies a need to search for patterns at many lengths [moen, etc].

% Another consequence is that we cannot simply discretize the time series and exploit string mining or frequent pattern mining techniques, since discretizing at the wrong length could obscure the pattern.

% In short, this task is plagued by three chicken-and-egg problems. To accurately infer the pattern, we must use 1) the right start positions, 2) the right instance lengths, and  3) the right dimensions. However, to know what any of these are, we (seemingly) must know the pattern.
% \begin{enumerate}
% \item We must infer the pattern using the right regions, but we can only select these regions if we know the pattern.
% \item We must search the right dimensions, but we only know these dimensions once we know the pattern.
% \item We must search at the right length, but we only know this length once we know the pattern.
% \end{enumerate}

% Fortunately, we describe a technique that bypasses all three of these problems. Through a unified feature selection task, our algorithm identifies the pattern, its regions, and the relevant dimensions in a constant number of passes over the data.
