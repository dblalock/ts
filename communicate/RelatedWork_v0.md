

    import numpy as np
    import matplotlib.pyplot as plt
    
    # enable imports; must be run from notebook's directory
    import sys
    sys.path.append('../python') # get access to all our code
    sys.path.append('../figs') # get access to all our figs
    sys.path.append('..') # stuff doing relative imports needs to be imported relative to here
    
    %matplotlib inline
    %load_ext autoreload
    %autoreload 2
    
    from utils import arrays as ar
    from datasets import synthetic as synth

## Related Work

In the previous section, we discussed several assumptions made and pitfalls
encountered by existing algorithms.
